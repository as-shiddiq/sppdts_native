-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2017 at 04:21 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_sppd7`
--

-- --------------------------------------------------------

--
-- Table structure for table `dasar`
--

CREATE TABLE IF NOT EXISTS `dasar` (
  `kd_dasar` int(11) NOT NULL,
  `ket_dasar` varchar(50) NOT NULL,
  PRIMARY KEY (`kd_dasar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dasar`
--

INSERT INTO `dasar` (`kd_dasar`, `ket_dasar`) VALUES
(1, 'UUD 45 pasal 28 F'),
(2, 'UU No.11 Th.2008 tentang Informasi dan Transaksi E'),
(3, 'UU No.14 Th.2008 tentang Keterbukaan Informasi Pub'),
(4, 'UU No.25 Th.2009 tentang Pelayanan Publik'),
(5, 'PP No.82 Th.2012 tentang Penyelenggaraan Sistem da'),
(6, 'INPRES No.6 Th.2001 tentang Kerangka Kebijakan Pen'),
(7, 'INPRES No.3 Th.2003 tentang Kebijakan dan Strategi');

-- --------------------------------------------------------

--
-- Table structure for table `detail_spt`
--

CREATE TABLE IF NOT EXISTS `detail_spt` (
  `no_spt` varchar(30) NOT NULL,
  `nip` bigint(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_spt`
--

INSERT INTO `detail_spt` (`no_spt`, `nip`) VALUES
('555/     /Diskominfo', 196112141984011001),
('srftertaete', 196112141984011013);

-- --------------------------------------------------------

--
-- Table structure for table `detail_ts`
--

CREATE TABLE IF NOT EXISTS `detail_ts` (
  `no_ts` varchar(30) NOT NULL,
  `kd_dasar` int(11) NOT NULL,
  KEY `id_ts` (`no_ts`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_ts`
--

INSERT INTO `detail_ts` (`no_ts`, `kd_dasar`) VALUES
('454545', 5),
('454545', 6),
('ererer', 7),
('erererere', 6),
('dfdfdfd', 6),
('123344444', 6),
('123', 7),
('123', 6),
('dfsds', 7),
('dfsds', 6),
('dfdf', 7),
('dfd', 6),
('dfd', 7),
('asd', 6),
('fsdf', 6),
('fsdf', 5);

-- --------------------------------------------------------

--
-- Table structure for table `instansi`
--

CREATE TABLE IF NOT EXISTS `instansi` (
  `kd_instansi` int(11) NOT NULL,
  `nm_instansi` varchar(50) NOT NULL,
  `tempat` varchar(25) NOT NULL,
  `alamat` varchar(20) NOT NULL,
  `nickname` varchar(40) NOT NULL,
  `kepala_dinas` varchar(100) NOT NULL,
  `NIP` varchar(100) NOT NULL,
  PRIMARY KEY (`kd_instansi`),
  KEY `kd_instansi` (`kd_instansi`),
  KEY `kd_instansi_2` (`kd_instansi`),
  KEY `kd_instansi_3` (`kd_instansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instansi`
--

INSERT INTO `instansi` (`kd_instansi`, `nm_instansi`, `tempat`, `alamat`, `nickname`, `kepala_dinas`, `NIP`) VALUES
(1, 'Sekretariat DPRD', 'Pelaihari', 'Jl. Akhmad Syairani', '', '', ''),
(2, 'Sekretariat Daerah', 'Pelaihari', 'Jl. Akhmad Syairani', '', '', ''),
(3, 'Dinas Komunikasi Dan Informatika', 'Pelaihari', 'Jl. Akhmad Syairani', 'Diskominfo', 'Andris Evony, S.STP.,M.Si', '1121213313'),
(4, 'Badan Penanggulangan Bencana Daerah', 'Pelaihari', 'Jl. Akhmad Syairani', '', '', ''),
(5, 'Dinas Kesehatan', 'Pelaihari', 'Jl. Akhmad Syairani', '', '', ''),
(6, 'Dinas Perhubungan', 'Pelaihari', 'Jl. Akhmad Syairani', 'Dishub', '', ''),
(7, 'Badan Kepegawaian Daerah', 'Pelaihari', 'Jl. Akhmad Syairani', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `nip` bigint(25) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `tingkat` varchar(20) NOT NULL,
  `golongan` varchar(10) NOT NULL,
  `pangkat` varchar(10) NOT NULL,
  `kd_instansi` int(11) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nip`, `nama`, `jabatan`, `tingkat`, `golongan`, `pangkat`, `kd_instansi`) VALUES
(11, 'ww', 'ww', '', 'ss', 'ss', 1),
(196112141984011001, 'SYAFRIANI ,SKM', 'Kepala Seksi', 'D', 'III', 'd', 5),
(196112141984011011, 'Andris Evony.S.STP.,M.Si', 'Kepala Dinas', '', '', '', 3),
(196112141984011013, 'Cindy Vinnalarika S.P', 'Kepala Dinas', 'D', 'II', 'D', 6),
(197107171993031011, 'KHAIRIL FAHMI ,S.Sos, MAP', 'Kepala Subbag', 'C', 'III', 'd', 1),
(197111032006041003, 'HERMIADI ,S.Sos', 'Kepala Seksi', 'D', 'III', 'c', 4),
(197611281996031003, 'ZAKI YAMANI, S.Pt', 'Kepala Bidang', 'C', 'III', 'd', 3),
(197806062008011026, 'MAHYUNI HIDAYAT, SE', 'Kepala Subbag', 'C', 'III', 'b', 2),
(198005042009031004, 'HADERIANSYAH, S.Sos', 'Kepala Seksi', 'D', 'III', 'b', 3),
(198011252002121003, 'DANOE SULAIMAN, SH', 'Kepala Bidang', 'C', 'III', 'c', 6),
(198303102009031004, 'DENNY WAHYUDHI, SE', 'Kepala Subbag', 'C', 'III', 'b', 2),
(198604092006021002, 'M. ALFAN ROSIDI ANWAR ,S.STP', 'Kepala Subbag', 'C', 'III', 'c', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai_disarankan`
--

CREATE TABLE IF NOT EXISTS `pegawai_disarankan` (
  `nip` text NOT NULL,
  `no_ts` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai_disarankan`
--

INSERT INTO `pegawai_disarankan` (`nip`, `no_ts`) VALUES
('197111032006041003', '123'),
('197111032006041003', 'dfsds'),
('197111032006041003', 'dfdf'),
('197111032006041003', 'dfd'),
('198005042009031004', 'asd'),
('196112141984011011', 'fsdf'),
('197611281996031003', 'fsdf'),
('198005042009031004', 'fsdf');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `level` enum('admin','user_instansi','user_persetujuan') NOT NULL,
  `nip` bigint(25) NOT NULL,
  `kd_instansi` int(12) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `nip` (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`username`, `password`, `level`, `nip`, `kd_instansi`) VALUES
('admin', 'admin', 'admin', 0, 1),
('khairil', '123456', 'user_persetujuan', 197107171993031011, 1),
('super', 'sekali', 'admin', 0, 0),
('user', '123456', 'user_instansi', 197107171993031011, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sppd`
--

CREATE TABLE IF NOT EXISTS `sppd` (
  `no_sppd` varchar(30) NOT NULL,
  `no_ts` varchar(30) NOT NULL,
  `no_spt` varchar(30) NOT NULL,
  PRIMARY KEY (`no_sppd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spt`
--

CREATE TABLE IF NOT EXISTS `spt` (
  `no_spt` varchar(30) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `transportasi` varchar(20) NOT NULL,
  `no_rek` int(20) NOT NULL,
  `no_ts` varchar(30) NOT NULL,
  PRIMARY KEY (`no_spt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spt`
--

INSERT INTO `spt` (`no_spt`, `tempat`, `tgl_mulai`, `tgl_selesai`, `transportasi`, `no_rek`, `no_ts`) VALUES
('ggggg', 'asd', '2017-07-01', '2017-07-03', 'gg', 4545, 'asd'),
('sdfsd', 'd', '2017-07-01', '2017-07-06', 'sers', 44545, 'fsdf');

-- --------------------------------------------------------

--
-- Table structure for table `tingkat_perjadin`
--

CREATE TABLE IF NOT EXISTS `tingkat_perjadin` (
  `tingkat` varchar(20) NOT NULL,
  `ket` varchar(60) NOT NULL,
  PRIMARY KEY (`tingkat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tingkat_perjadin`
--

INSERT INTO `tingkat_perjadin` (`tingkat`, `ket`) VALUES
('A', 'Bupati / Wakil Bupati / Sekertaris Daerah / Pimpinan DPRD'),
('B', 'Anggota DPRD'),
('C', 'Pejabat Eselon II dan Kepala SKPD'),
('D', 'Pejabat Eselon III'),
('E', 'Pejabat Eselon IV'),
('F', 'Staff Pelaksana Gol I, II, III'),
('G', 'PTT dan Personil Non Pegawai');

-- --------------------------------------------------------

--
-- Table structure for table `ts`
--

CREATE TABLE IF NOT EXISTS `ts` (
  `no_ts` varchar(30) NOT NULL,
  `perihal` varchar(200) NOT NULL,
  `kepada` varchar(50) NOT NULL,
  `pengirim` varchar(25) NOT NULL,
  `tujuan` varchar(200) NOT NULL,
  `tempat_tujuan` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `isi` varchar(200) NOT NULL,
  `saran_tindakan` varchar(200) NOT NULL,
  `kesimpulan` varchar(200) NOT NULL,
  `penutup` varchar(50) NOT NULL,
  `kd_instansi` int(11) NOT NULL,
  `status` enum('Y','T','B') DEFAULT NULL,
  `program` varchar(50) NOT NULL,
  `kegiatan` varchar(50) NOT NULL,
  `no_rek` varchar(20) NOT NULL,
  PRIMARY KEY (`no_ts`),
  KEY `kd_instansi` (`kd_instansi`),
  KEY `kd_instansi_2` (`kd_instansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ts`
--

INSERT INTO `ts` (`no_ts`, `perihal`, `kepada`, `pengirim`, `tujuan`, `tempat_tujuan`, `tanggal`, `isi`, `saran_tindakan`, `kesimpulan`, `penutup`, `kd_instansi`, `status`, `program`, `kegiatan`, `no_rek`) VALUES
('123', 'tidak tahu menahu', 'Sekretariat Daerah', 'Kepala Mantap', 'tidak ada', 'surabaya', '0000-00-00', 'jika ini yang terbaik', 'tidak ada', 'tidak tau', 'jiah', 4, 'B', '', '', ''),
('asd', 'asd', 'Bupati Kabupaten Tanah Laut', 'asdas', 'asd', 'asd', '2017-07-06', 'asd', 'ad', 'asd', 'asd', 3, 'Y', 'sad', 'asd', '232'),
('dfd', 'dfdfd', 'Sekretariat Daerah', 'dfdf', 'dfd', 'fdfd', '2017-07-13', 'fdfd', 'df', 'fdfdf', 'fdfd', 3, 'B', 'dfd', 'dfdfd', '190.019101'),
('fsdf', 'as', 'Bupati Kabupaten Tanah Laut', 'sdf', 'sdas', 'd', '2017-07-08', 'asd', 'fdgd', 'sad', 'fcf', 3, 'Y', 'xdvx', 'jhkjh', '454');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
