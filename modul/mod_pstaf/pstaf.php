<script>
	$(document).ready(function(){
		$("#form_add").css("display","none");
		$("#add").click(function(){
			$("#form_add").fadeToggle(1000);

		});
	});
</script>
<script type="text/javascript">
		 $(document).ready(function() {
		  $("#txtcari").keyup(function() {
		   var strcari = $("#txtcari").val();
		   if (strcari != "")
		   {
		   $("#tabel_awal").css("display", "none");

			$("#hasil").html("<img src='images/loader.gif'/>")
			$.ajax({
			 type:"post",
			 url:"modul/mod_pstaf/cari.php",
			 data:"q="+ strcari,
			 success: function(data){
			 $("#hasil").css("display", "block");
			  $("#hasil").html(data);
			  
			 }
			});
		   }
		   else{
		   $("#hasil").css("display", "none");
		   $("#tabel_awal").css("display", "block");
		   }
		  });
			});
	</script>
<?php	
$aksi="modul/mod_pstaf/aksi_pstaf.php";
$judul="Data Pegawai";
switch(isset($_GET['act'])){
	default:
	$p      = new Paging;
    $batas  = 10;
    $posisi = $p->cariPosisi($batas);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>
					</div>
                    <div class="clearfix"></div>


                    <div class="row">
                                <div class="x_content">

										<div class="col-md-6 col-sm-6 col-xs-12">
											<button type="button" class="btn btn-default btn-sm" id="add"><i class="fa fa-plus-circle"></i> Tambah Data Pegawai</button>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" required="required" class="form-control" placeholder="Search" id="txtcari">
                                        </div>
									<div class="row" id="form_add">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Pegawai</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form action="<?php echo $aksi."?module=add" ?>" method="post" data-parsley-validate class="form-horizontal form-label-left">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="nama" type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">NIP
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="nip" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Jabatan </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="jabatan">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tingkat
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select class="form-control col-md-7 col-xs-12" id="selec" name="kd_instansi">
                                                        <option disabled selected>Pilih</option>
                                                            <?php
                                                                $query=mysql_query("SELECT * FROM tingkat_perjadin");
                                                                while($r=mysql_fetch_array($query)){
                                                            ?>
                    
                                                        <option value="<?php echo $r['tingkat'] ?>" ><?php echo $r['tingkat'] ?></option>

                                                        <?php
                                                        }?>
                                                        </select>
                                                    </div>
                                        </div>
										<div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Golongan </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                (I/a s/d IV/d)
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="golongan">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Pangkat </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="pangkat">
                                            </div>
                                        </div>
										<div class="form-group">
                                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select class="form-control col-md-7 col-xs-12" id="selec" name="kd_instansi">
                                                        <option disabled selected>Pilih</option>
                                                            <?php
                                                                $query=mysql_query("SELECT * FROM instansi");
                                                                while($r=mysql_fetch_array($query)){
                                                            ?>
                    
                                                        <option value="<?php echo $r['kd_instansi'] ?>" ><?php echo $r['nm_instansi'] ?></option>

                                                        <?php
                                                        }?>
                                                        </select>
                                                    </div>
                                                    
                                                </div>
                                        
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
									<div id="hasil"></div>
									<div id="tabel_awal">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                               
                                                <th>Nama</th>
                                                <th>NIP</th>
                                                <th>Jabatan</th>
                                                <th>Tingkat</th>
												<th>Golongan</th>
                                                <th>Pangkat</th>
												<th>Instansi</th>

												<th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
                                            $where = $_SESSION['kd_instansi'];
											$query=@mysql_query("SELECT * FROM pegawai LEFT JOIN instansi ON pegawai.kd_instansi=instansi.kd_instansi WHERE pegawai.kd_instansi=$where");

                                            if ($query === FALSE){
                                                die(mysql_error());
                                            }
											//$no=1;
											while($r=@mysql_fetch_array($query)){
										?>
                                            <tr>
                                               
                                                <td><?php echo $r['nama']; ?></td>
                                                <td><?php echo $r['nip']; ?></td>
                                                <td><?php echo $r['jabatan']; ?></td>
												<td><?php echo $r['tingkat']; ?></td>
												<td><?php echo $r['golongan']; ?></td>
                                                <td><?php echo $r['pangkat']; ?></td>
												<td><?php echo $r['nm_instansi']; ?></td>
												<th>
													<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Update" onclick="window.location='media.php?module=pstaf&&act=update&&nip=<?php echo $r['nip']; ?>'"><i class="fa fa-retweet"></i> Update</button>
													<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" onclick="window.location='<?php echo $aksi."?module=delete&&nip=".$r['nip']; ?>'"><i class="fa fa-trash"></i> Delete</button>
												</th>
                                            </tr>
										<?php
										
										}
										?>
                                        </tbody>
                                    </table>
									</div>
									<?php
							$jmldata=@mysql_num_rows(mysql_query("SELECT * FROM pegawai"));
							$jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
							$linkHalaman = $p->navHalaman($_GET['halaman'], $jmlhalaman); 
							echo "$linkHalaman";
							
							?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;

case "update":
$nip=$_GET['nip'];
$query=mysql_query("SELECT * FROM pegawai WHERE nip='$nip'");
$r=mysql_fetch_array($query);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>

                     
                    </div>
                    <div class="clearfix"></div>


                   <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Update <small><?php echo $r['nama']; ?></small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $aksi."?module=update"; ?>">
									<input type="hidden" name="id" value="<?php echo $r['nip']; ?>">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="nama" type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['nama']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">NIP
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="nip" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['nip']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Jabatan </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="jabatan" value="<?php echo $r['jabatan']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tingkat </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="tingkat" value="<?php echo $r['pangkat']; ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Golongan </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="golongan" value="<?php echo $r['golongan']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Pangkat </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="pangkat" value="<?php echo $r['pangkat']; ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="instansi" value="<?php echo $r['kd_instansi']; ?>">
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;
}
?>