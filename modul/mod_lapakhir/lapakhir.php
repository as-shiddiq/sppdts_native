<script>
	$(document).ready(function(){
		$("#form_add").css("display","none");
		$("#add").click(function(){
			$("#form_add").fadeToggle(1000);

		});
	});
</script>
<script type="text/javascript">
		 $(document).ready(function() {
		  $("#txtcari").keyup(function() {
		   var strcari = $("#txtcari").val();
		   if (strcari != "")
		   {
		   $("#tabel_awal").css("display", "none");

			$("#hasil").html("<img src='images/loader.gif'/>")
			$.ajax({
			 type:"post",
			 url:"modul/mod_admin/cari.php",
			 data:"q="+ strcari,
			 success: function(data){
			 $("#hasil").css("display", "block");
			  $("#hasil").html(data);
			  
			 }
			});
		   }
		   else{
		   $("#hasil").css("display", "none");
		   $("#tabel_awal").css("display", "block");
		   }
		  });
			});
	</script>
	<style>
	#selec{
		width:500px;
	}
	</style>
<?php	
$aksi="modul/mod_admin/aksi_admin.php";
switch(isset($_GET['act'])){
	default:
	$p      = new Paging;
    $batas  = 10;
    $posisi = $p->cariPosisi($batas);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>


                    <div class="row">
                                <div class="x_content">

										
										
									
									<div id="tabel_awal">
                                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Rekap Laporan Berdasarkan :</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form action="laporan/lapakhir/f_lapakhir.php" method="get" data-parsley-validate class="form-horizontal form-label-left">

                   
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tahun Anggaran
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control" name="mulai" data-inputmask="'mask': '9999'">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <fieldset>
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left" name="tgl1" id="single_cal2" placeholder="Masukkan Tanggal" aria-describedby="inputSuccess2Status2">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                        </div>
                                                    </div>
                                                </div>
												</fieldset>
												
											 <fieldset>
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left" name="tgl2" id="single_cal3" placeholder="Masukkan Tanggal" aria-describedby="inputSuccess2Status3">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            </div>
                                        </div>
										
										
										
										<div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Bagian
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-5">
                                                        <select class="select2_single form-control" tabindex="-1" name="bagian" id="selec">
														 <option disabled selected name="t2">Pilih</option>
															
															
															<option value="KAUKU">KAUKU</option>
															<option value="P2BU">P2BU</option>
															<option value="TU">TU</option>
															
														</select>
                                                    </div>
													
                                                </div>
												
										
										<div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pejabat Pembuat Komitmen
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-5">
                                                        <select class="select2_single form-control" tabindex="-1" name="kom" id="selec">
														 <option disabled selected name="t2">Pilih</option>
															<?php
																$query=mysql_query("SELECT * FROM tbl_pegawai");
																while($r=mysql_fetch_array($query)){
															?>
															
															<option value="<?php echo $r['id_pegawai'] ?>"><?php echo $r['nama']." / ".$r['pangkat']." (".$r['pangkat'].")"; ?></option>
															<?php
															}
															?>
														</select>
                                                    </div>
													
                                                </div>
                                        
                                        <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kuasa Pengguna Anggaran
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-5">
                                                        <select class="select2_single form-control" tabindex="-1" name="kua" id="selec">
														 <option disabled selected name="t2">Pilih</option>
															<?php
																$query=mysql_query("SELECT * FROM tbl_pegawai");
																while($r=mysql_fetch_array($query)){
															?>
															
															<option value="<?php echo $r['id_pegawai'] ?>"><?php echo $r['nama']." / ".$r['pangkat']." (".$r['pangkat'].")"; ?></option>
															<?php
															}
															?>
														</select>
                                                    </div>
													
                                                </div>
										<input type="hidden" value="<?php echo $idLog; ?>" name="onlog">
                                       
												
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
									</div>
									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;

case "update":
$id_admin=$_GET['id_admin'];
$query=mysql_query("SELECT * FROM tbl_admin, tbl_pegawai WHERE tbl_admin.id_admin='$id_admin' AND tbl_admin.id_peg=tbl_pegawai.id_pegawai");
$r=mysql_fetch_array($query);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>

                     
                    </div>
                    <div class="clearfix"></div>


                   <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Update <small><?php echo $r['nama']; ?></small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $aksi."?module=update"; ?>">
									<input type="hidden" name="id" value="<?php echo $r['id_admin']; ?>">
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="username" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['username']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span> </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="password" name="pass"  value="<?php echo $r['password']; ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Retry Password <span class="required">*</span> </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="password" name="rpass"  value="<?php echo $r['password']; ?>">
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;
}
?>