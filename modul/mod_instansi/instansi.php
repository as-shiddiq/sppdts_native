<script>
    $(document).ready(function(){
        $("#form_add").css("display","none");
        $("#add").click(function(){
            $("#form_add").fadeToggle(1000);

        });
    });
</script>
<script type="text/javascript">
         $(document).ready(function() {
          $("#txtcari").keyup(function() {
           var strcari = $("#txtcari").val();
           if (strcari != "")
           {
           $("#tabel_awal").css("display", "none");

            $("#hasil").html("<img src='images/loader.gif'/>")
            $.ajax({
             type:"post",
             url:"modul/mod_instansi/cari.php",
             data:"q="+ strcari,
             success: function(data){
             $("#hasil").css("display", "block");
              $("#hasil").html(data);
              
             }
            });
           }
           else{
           $("#hasil").css("display", "none");
           $("#tabel_awal").css("display", "block");
           }
          });
            });
    </script>
    <style>
    
    </style>
<?php   
$aksi="modul/mod_instansi/aksi_instansi.php";
$judul="Instansi";
switch(isset($_GET['act'])){
    default:
    $p      = new Paging;
    $batas  = 10;
    $posisi = $p->cariPosisi($batas);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="row">
                                <div class="x_content">

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <button type="button" class="btn btn-default btn-sm" id="add"><i class="fa fa-plus-circle"></i> Tambah Instansi</button>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" required="required" class="form-control" placeholder="Search" id="txtcari">
                                        </div>
                                    <div class="row" id="form_add">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Instansi</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form action="<?php echo $aksi."?module=add" ?>" method="post" data-parsley-validate class="form-horizontal form-label-left">
                                    <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kode Instansi<span class="required"></span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="kd_instansi" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Instansi<span class="required"></span> </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nm_instansi">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tempat<span class="required"></span> </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="tempat">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat<span class="required"></span> </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="alamat">
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                                    <div id="hasil"></div>
                                    <div id="tabel_awal">
                                    <?php
                                        if($level=='superadmin'){
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Kode Instansi</th>
                                                <th>Nama Instansi</th>
                                                <th>Tempat</th>
                                                <th>Alamat</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $query=mysql_query("SELECT * FROM instansi WHERE instansi.kd_instansi ORDER BY instansi.kd_instansi");
                                            
                                            if ($query === FALSE){
                                                die(mysql_error());
                                            }
                                            //$no=1;
                                            while($r=mysql_fetch_array($query)){
                                        ?>
                                            <tr>
                                               
                                                <td><?php echo $r['kd_instansi']; ?></td>
                                                <td><?php echo $r['nm_instansi']; ?></td>
                                                <td><?php echo $r['tempat']; ?></td>
                                                <td><?php echo $r['alamat']; ?></td>
                                                <th>
                                                    <button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Update" onclick="window.location='media.php?module=instansi&&act=update&&kd_instansi=<?php echo $r['kd_instansi']; ?>'"><i class="fa fa-retweet"></i> Update</button>
                                                    <button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" onclick="window.location='<?php echo $aksi."?module=delete&&kd_instansi=".$r['kd_instansi']; ?>'"><i class="fa fa-trash"></i> Delete</button>
                                                </th>
                                            </tr>
                                        <?php
                                        
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <?php
                                    }
                                    else{
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Kode Instansi</th>
                                                <th>Nama Instansi</th>
                                                <th>Tempat</th>
                                                <th>Alamat</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $query=mysql_query("SELECT * FROM instansi WHERE instansi.kd_instansi ORDER BY instansi.kd_instansi");
                                            $num=mysql_num_rows($query);
                                            $no=1;
                                            while($r=mysql_fetch_array($query)){
                                        ?>
                                            <tr>
                                               
                                                <td><?php echo $r['kd_instansi']; ?></td>
                                                <td><?php echo $r['nm_instansi']; ?></td>
                                                <td><?php echo $r['tempat']; ?></td>
                                                <td><?php echo $r['alamat']; ?></td>
                                                <th>
                                                    <button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Update" onclick="window.location='media.php?module=instansi&&act=update&&kd_instansi=<?php echo $r['kd_instansi']; ?>'"><i class="fa fa-retweet"></i> Update</button>
                                                    </th>
                                            </tr>
                                        <?php
                                        
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <?php
                                    }
                            
                                    ?>
                                    </div>
                                    <?php
                            $jmldata=mysql_num_rows(mysql_query("SELECT * FROM instansi"));
                            $jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
                            $linkHalaman = $p->navHalaman($_GET['halaman'], $jmlhalaman); 
                            echo "$linkHalaman";
                            
                            ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;

case "update":
$kd_instansi=$_GET['kd_instansi'];
$query=mysql_query("SELECT * FROM instansi WHERE instansi.kd_instansi='$kd_instansi'");
$r=mysql_fetch_array($query);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>

                     
                    </div>
                    <div class="clearfix"></div>


                   <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Update <small><?php echo $r['kd_instansi']; ?></small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $aksi."?module=update"; ?>">
                                    <input type="hidden" name="id" value="<?php echo $r['instansi']; ?>">
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kode Instansi <span class="required"></span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="kd_instansi" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['kd_instansi']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Instansi <span class="required"></span> </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nm_instansi"  value="<?php echo $r['nm_instansi']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tempat <span class="required"></span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="tempat" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['tempat']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat <span class="required"></span> </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="alamat"  value="<?php echo $r['alamat']; ?>">
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;
}
?>