<script>
	$(document).ready(function(){
		$("#form_add").css("display","none");
		$("#add").click(function(){
			$("#form_add").fadeToggle(1000);

		});
	});
</script>
<script type="text/javascript">
		 $(document).ready(function() {
		  $("#txtcari").keyup(function() {
		   var strcari = $("#txtcari").val();
		   if (strcari != "")
		   {
		   $("#tabel_awal").css("display", "none");

			$("#hasil").html("<img src='images/loader.gif'/>")
			$.ajax({
			 type:"post",
			 url:"modul/mod_biayataksi/cari.php",
			 data:"q="+ strcari,
			 success: function(data){
			 $("#hasil").css("display", "block");
			  $("#hasil").html(data);

			 }
			});
		   }
		   else{
		   $("#hasil").css("display", "none");
		   $("#tabel_awal").css("display", "block");
		   }
		  });
			});
	</script>
<?php
$aksi="modul/mod_kegiatan/aksi_kegiatan.php";
$judul="Telaahan Staff";
switch(isset($_GET['act'])){
	default:
	$p      = new Paging;
    $batas  = 10;
    $posisi = $p->cariPosisi($batas);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>
					</div>
                    <div class="clearfix"></div>


                    <div class="row">
                                <div class="x_content">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<button type="button" class="btn btn-default btn-sm" id="add"><i class="fa fa-plus-circle"></i> Tambah Telaahan Staff</button>
									</div>

									<div class="row" id="form_add">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Telaahan Staff</h2>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form action="<?php echo $aksi."?module=add" ?>" method="post" data-parsley-validate class="form-horizontal form-label-left">
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nomor Surat
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="no_ts" class="form-control" value="">
                                                <span class="fa fa-sort-numeric-asc form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
										<div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kepada
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                       <select class="form-control col-md-7 col-xs-12" id="selec" name="kepada">
                                                        <option disabled selected>Pilih</option>
                                                        <option value="Bupati Kabupaten Tanah Laut"> Bupati Kabupaten Tanah Laut </option>
                                                        <!-- <option value="Wakil Bupati Kabupaten Tanah Laut"> Wakil Bupati Kabupaten Tanah Laut </option> -->
                                                        <option value="Sekretariat Daerah"> Sekretariat Daerah </option>
                                                        </select>
                                                    </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Dari
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="pengirim" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="date" class="form-control" name="tanggal" data-inputmask="">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Perihal
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="perihal" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Dasar</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select class="form-control" id="dasar" name="dasar[]" multiple="multiple">
                                                        <option value="">Pilih</option>
                                                            <?php
                                                            // ambil data dari database
                                                            $query2 = mysql_query("SELECT * FROM dasar ORDER BY ket_dasar");
                                                            while ($row2 = mysql_fetch_array($query2)) {
                                                                ?>
                                                                <option value="<?php echo $row2['kd_dasar'] ?>"><?php echo $row2['ket_dasar'] ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tujuan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="tujuan" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tempat Tujuan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="tempat_tujuan" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Isi
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="isi" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kesimpulan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="kesimpulan" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Saran dan Tindakan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="saran_tindakan" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No Rekening
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="no_rek" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Program
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="program" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kegiatan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="kegiatan" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Penutup
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="penutup" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pegawai Yang Diusulkan</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-5">
                                                        <select class="form-control" id="pegawai" name="pegawai[]" multiple="multiple">
                                                        <option value="">Pilih</option>
                                                            <?php
                                                            // ambil data dari database
                                                            $query3 = mysql_query("SELECT * FROM pegawai WHERE kd_instansi='$kd_instansi' ORDER BY nip");
                                                            while ($row3 = mysql_fetch_array($query3)) {
                                                                ?>
                                                                <option value="<?php echo $row3['nip'] ?>"><?php echo $row3['nama'] ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
							             <!--div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="status" class="form-control">
                                                    <option value="Y">IYA</option>
                                                    <option value="T">TIDAK</option>
                                                </select>
                                            </div>
                                        </div-->
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
									<div id="hasil"></div>
									<div id="tabel_awal">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">No</th>
                                                <th width="20%" rowspan="2">NO SURAT</th>
                                                <th rowspan="2">TUJUAN</th>
												<th rowspan="2">ISI</th>
												<th rowspan="2">TANGGAL</th>
                                                <th rowspan="2">STATUS</th>
                                                <th rowspan="2">AKSI</th>

											</tr>

                                        </thead>

                                        <tbody>
										<?php
											$query=mysql_query("SELECT * FROM ts WHERE kd_instansi='$kd_instansi'");
											$num=@mysql_num_rows($query);
											$no=1;
											while($r=@mysql_fetch_array($query)){
										?>
                                            <tr>

                                                <td><?php echo $no; ?></td>

												<td><?php echo $r['no_ts']; ?></td>
                                                <td><?php echo $r['tujuan']; ?></td>
												<td><?php echo $r['isi']; ?></td>
                                                <td><?php echo $r['tanggal']; ?></td>
                                                <td>
                                                <?php if ($r['status']=='B'){ ?>
                                                    <a href="#" type="button" class="btn btn-primary">Belum Disetujui</a>
                                                    <?php
                                                    }
                                                     elseif ($r['status']=='Y'){ ?>
                                                     <a href="#" type="button" class="btn btn-success">Disetujui</a>
                                                    <?php    }
                                                    else { ?>
                                                    <a href="#" type="button" class="btn btn-danger">Tidak Disetujui</a>

                                                       <?php }
                                                ?>

                                                </td>

												<th>
													<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Update" onclick="window.location='media.php?module=kegiatan&&act=update&&no_ts=<?php echo $r['no_ts']; ?>'"><i class="fa fa-retweet"></i> Update</button>

												</th>
                                            </tr>
										<?php
										$no++;
										}
										?>
                                        </tbody>
                                    </table>
									</div>
									<?php
							$jmldata=@mysql_num_rows(mysql_query("SELECT * FROM ts"));
							$jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
							$linkHalaman = $p->navHalaman($_GET['halaman'], $jmlhalaman);
							echo "$linkHalaman";

							?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;

case "update":
$no_ts = $_GET['no_ts'];
$query=mysql_query("SELECT * FROM ts WHERE ts.no_ts = '$no_ts'");
$r=mysql_fetch_array($query);
$queryy=mysql_query("SELECT * FROM detail_ts WHERE no_ts = '$no_ts'");
$y=mysql_fetch_array($queryy);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>


                    </div>
                    <div class="clearfix"></div>


                   <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Update <small><?php echo $r['no_ts']; ?></small></h2>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $aksi."?module=update"; ?>">
									<input type="hidden" name="no_ts" value="<?php echo $r['no_ts']; ?>">
									<input type="hidden" id="last-name" name="nip" value="<?php echo $username ?>" class="form-control col-md-7 col-xs-12">
									<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nomor Surat
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="no_ts" class="form-control" value="<?php echo $r['no_ts'] ?>">
                                                <input type="hidden" name="def_no_ts" class="form-control" value="<?php echo $r['no_ts'] ?>">

                                            </div>
                                        </div>

										 <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kepada
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">


                                                 <select class="form-control col-md-7 col-xs-12" id="selec" name="kepada">
                                                       <option disabled selected>Pilih</option>
																											 <?php
																											 	$bupati='';
																												$sekda='';
																											 	if($r['kepada']=='Bupati Kabupaten Tanah Laut'){
																													$bupati='selected';
																												}
																												else{
																													$sekda='selected';
																												}
																											  ?>
                                                        <option value="Bupati Kabupaten Tanah Laut" <?=$bupati?>> Bupati Kabupaten Tanah Laut </option>
                                                        <!-- <option value="WakilBupati Kabupaten Tanah Laut"> Wakil Bupati Kabupaten Tanah Laut </option> -->
                                                        <option value="Sekretariat Daerah" <?=$sekda?>> Sekretariat Daerah </option>
                                                        </select>
											</div>
                                        </div>

										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Dari
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="pengirim" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['pengirim'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="date" class="form-control" name="tanggal" data-inputmask="" required="required" value="<?php echo $r['tanggal'] ?>">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Perihal
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="perihal" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['perihal'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Dasar</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <?php
                                                        $sql="SELECT kd_dasar FROM detail_ts WHERE no_ts='".$no_ts."'";
                                                        $ambilts=mysql_query($sql) or die(mysql_error());
                                                        $array=null;
                                                        while ($f=mysql_fetch_array($ambilts)) {
                                                            $array[$f['kd_dasar']]=$f['kd_dasar'];
                                                        }
                                                 ?>
                                                <select class="form-control" id="dasar" name="dasar[]" multiple="multiple">
                                                        <option value="">Pilih</option>
                                                        <?php
                                                        // ambil data dari database
                                                        $query2 = mysql_query("SELECT * FROM dasar");
                                                        while ($row2 = mysql_fetch_array($query2)) {
                                                          if( array_key_exists($row2['kd_dasar'], $array)){
                                                                        ?>
                                                        <option value="<?php echo $row2['kd_dasar'] ?>" selected><?php echo $row2['ket_dasar'] ?></option>
                                                                        <?php
                                                                }
                                                                else{
                                                                    ?>
                                                        <option value="<?php echo $row2['kd_dasar'] ?>"><?php echo $row2['ket_dasar'] ?></option>
                                                                <?php
                                                                        }
                                                        ?>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tujuan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="tujuan" rows="3"><?php echo $r['tujuan']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tempat Tujuan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="tempat_tujuan" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['tempat_tujuan'] ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Isi
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="isi" rows="3"><?php echo $r['isi']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kesimpulan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="kesimpulan" rows="3"><?php echo $r['kesimpulan']?></textarea>
                                            </div>
                                        </div>

										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Saran dan Tindakan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="saran_tindakan" rows="3"><?php echo $r['saran_tindakan']?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No Rekening
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="no_rek" class="form-control" value="<?=$r['no_rek']?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Program
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="program" rows="3"><?php echo $r['program']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kegiatan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="kegiatan" rows="3"><?php echo $r['kegiatan']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Penutup
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="penutup" rows="3"><?php echo $r['penutup']?></textarea>
                                            </div>
                                        </div>
                                           <div class="form-group">
                                           <?php
                                                        $sql="SELECT nip FROM pegawai_disarankan WHERE no_ts='".$no_ts."'";
                                                        $ambilpegawai=mysql_query($sql) or die(mysql_error());
                                                        $array=null;
                                                        while ($f=mysql_fetch_array($ambilpegawai)) {
                                                            $array[$f['nip']]=$f['nip'];
                                                        }
                                           ?>
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pegawai Yang Diusulkan</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-5">
                                                        <select class="form-control" id="pegawai" name="pegawai[]" multiple="multiple">
                                                        <option value="">Pilih</option>
                                                            <?php
                                                            // ambil data dari database
                                                            $query3 = mysql_query("SELECT * FROM pegawai WHERE kd_instansi='$kd_instansi' ORDER BY nip");
                                                            while ($row3 = mysql_fetch_array($query3)) {
                                                                    if( array_key_exists($row3['nip'], $array)){
                                                                            ?>
                                                                                <option value="<?php echo $row3['nip'] ?>" selected><?php echo $row3['nama'] ?></option>
                                                                            <?php
                                                                    }
                                                                    else{
                                                                        ?>
                                                                                <option value="<?php echo $row3['nip'] ?>"><?php echo $row3['nama'] ?></option>
                                                                    <?php
                                                                            }
                                                                ?>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                         <!--div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="status" class="form-control">
                                                    <option value="Y">IYA</option>
                                                    <option value="T">TIDAK</option>
                                                </select>
                                            </div>
                                        </div-->

                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;
}
?>

        <script src="../../js/select2-master/dist/js/select2.min.js"></script>
        <script>
            $(document).ready(function () {
                $("#dasar").select2({
                    placeholder: "Please Select"
                });
                $("#pegawai").select2({
                    placeholder: "Please Select"
                });
                 $("#dasar2 ").select2({
                    placeholder: "Please Select"
                });
            });
        </script>
