<div class="right_col" role="main">


                


                <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><i class="fa fa-dashboard"></i> Laporan <small>Sistem Informasi Surat Perjalanan Dinas</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <div class="col-md-6 col-lg-6 col-sm-7">
                                        <!-- blockquote -->
									
										 <blockquote>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/spd/f_spd.php'"><i class="fa fa-file-pdf-o"></i> SURAT PERINTA DINAS</button>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/bpb/f_bpb.php'"><i class="fa fa-file-pdf-o"></i> BUKTI PENYERAHAN BERKAS</button>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/vbpd/f_vbpd.php'"><i class="fa fa-file-pdf-o"></i> VERIFIKASI BIAYA PERJALANAN DINAS</button>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/sp/f_sp.php'"><i class="fa fa-file-pdf-o"></i> SURAT PERNYATAAN</button>
                                        </blockquote>
										
										 <blockquote>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/spd/f_spd.php'"><i class="fa fa-file-pdf-o"></i> SURAT PERJALANAN DINAS (SPD)</button>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/dpr/f_dpr.php'"><i class="fa fa-file-pdf-o"></i> DAFTAR PENGELUARAN RIIL</button><br>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/menku/f_menku.php'"><i class="fa fa-file-pdf-o"></i> SURAT PENGESAHAN</button>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/sppt/f_sppt.php'"><i class="fa fa-file-pdf-o"></i> SURAT PERNYATAAN PEMBATALAN TUGAS PERJALANAN DINAS </button>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/spp/f_spp.php'"><i class="fa fa-file-pdf-o"></i> SURAT PERNYATAAN PEMBEBANAN BIAYA PEMBATALAN PERJALANAN DINAS</button>
										</blockquote>
                                        
										 <blockquote>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/kwitansi/f_kwitansi.php'"><i class="fa fa-file-pdf-o"></i> KWITANSI (UP)</button>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/form_kehadiran/f_fk.php'"><i class="fa fa-file-pdf-o"></i> FORM BUKTI KEHADIRAN PELAKSANAAN PERJALANAN DINAS</button>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/rbpd/f_rbpd.php'"><i class="fa fa-file-pdf-o"></i> RINCIAN BIAYA PERJALANAN DINAS</button>
											<button class="btn btn-primary" style="margin-right: 5px;" onclick="window.location='laporan/monitoring/f_monitoring.php'"><i class="fa fa-file-pdf-o"></i> MONITORING PENERBITAN SURAT TUGAS</button>
										</blockquote>
                                    </div>
                                   
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>