<script>
	$(document).ready(function(){
		$("#form_add").css("display","none");
		$("#add").click(function(){
			$("#form_add").fadeToggle(1000);

		});
	});
</script>
<script type="text/javascript">
		 $(document).ready(function() {
		  $("#txtcari").keyup(function() {
		   var strcari = $("#txtcari").val();
		   if (strcari != "")
		   {
		   $("#tabel_awal").css("display", "none");

			$("#hasil").html("<img src='images/loader.gif'/>")
			$.ajax({
			 type:"post",
			 url:"modul/mod_biayataksi/cari.php",
			 data:"q="+ strcari,
			 success: function(data){
			 $("#hasil").css("display", "block");
			  $("#hasil").html(data);
			  
			 }
			});
		   }
		   else{
		   $("#hasil").css("display", "none");
		   $("#tabel_awal").css("display", "block");
		   }
		  });
			});
	</script>
<?php	
$aksi="modul/mod_kegiatan/aksi_kegiatan.php";
$judul="Telaahan Staff";
switch(isset($_GET['act'])){
	default:
	$p      = new Paging;
    $batas  = 10;
    $posisi = $p->cariPosisi($batas);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>
					</div>
                    <div class="clearfix"></div>


                    <div class="row">
                                <div class="x_content">
									
									
									<div class="row" id="form_add">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Telaahan Staff</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form action="<?php echo $aksi."?module=add" ?>" method="post" data-parsley-validate class="form-horizontal form-label-left">
										<input type="hidden" id="last-name" name="nip" value="<?php echo $nip ?>" class="form-control col-md-7 col-xs-12">
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nomor Surat
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="no_ts" class="form-control" value="KP.004/DISKOMINFO.0000/XX/2015">
                                                <span class="fa fa-sort-numeric-asc form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
										<div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kepada
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select class="form-control col-md-7 col-xs-12" id="selec" name="kepada">
                                                        <option disabled selected>Pilih</option>
                                                        <option value="A"> Bupati Kabupaten Tanah Laut </option>
                                                        <option value="B"> Sekretariat Daerah </option>
                                                        </select>
                                                    </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Dari
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="pengirim" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control" name="tanggal" data-inputmask="'mask': '99/99/9999'">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Perihal 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="t3" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Dasar
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" name="dasar" value="">
                                            <input type="text" class="form-control" name="dasar" value="">
                                            <input type="text" class="form-control" name="dasar" value="">
                                            <input type="text" class="form-control" name="dasar" value="">
                                            <input type="text" class="form-control" name="dasar" value="">
                                            <input type="text" class="form-control" name="dasar" value="">
                                            <input type="text" class="form-control" name="dasar" value="">
                                            <input type="text" class="form-control" name="dasar" value="">
                                            <input type="text" class="form-control" name="keter3" value="">
                                            <input type="text" class="form-control" name="keter3" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tujuan 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="tujuan" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tempat Tujuan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="tempat_tujuan" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Isi 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="isi" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kesimpulan 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="kesimpulan" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Saran dan Tindakan 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="saran_tindakan" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Penutup 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="penutup" rows="3"></textarea>
                                            </div>
                                        </div>
										<!--div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tempat Tujuan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="tempat_tujuan" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
										
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Keperluan 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="t3" rows="3"></textarea>
                                            </div>
                                        </div>
                                        
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Berkendaraan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t4" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Berangkat Tanggal
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control" name="t5" data-inputmask="'mask': '99/99/9999'">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Mulai
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control" name="t6" data-inputmask="'mask': '99/99/9999'">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Selesai
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control" name="t7" data-inputmask="'mask': '99/99/9999'">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tahun Anggaran
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control" name="t_ang" data-inputmask="'mask': '9999'">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Keterangan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
<input type="text" class="form-control" name="t8" value="1. Selesai tugas harap melaporkan hasil kepada pimpinan.">
<input type="text" class="form-control" name="keter2" value="2. Biaya Perjalanan dibebankan pada DPA Diskominfo 2017">
<input type="text" class="form-control" name="keter3" value="">
                                            </div>
                                        </div-->                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
									<div id="hasil"></div>
									<div id="tabel_awal">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">No</th>
                                                <th width="20%" rowspan="2">TUJUAN</th>
                                                <th rowspan="2">NOMOR SURAT</th>
												<th rowspan="2">ISI</th>
                                                <th rowspan="2">TANGGAL</th>
                                                <th rowspan="2">KONFIRMASI</th>
											</tr>
											
                                        </thead>
                                        <tbody>
										<?php

                                            $query=mysql_query("SELECT * FROM ts a LEFT JOIN instansi b ON a.kepada='Sekretariat Daerah' WHERE b.kd_instansi = a.kd_instansi AND status='B'");

											//$query=mysql_query("SELECT * FROM ts a LEFT JOIN instansi b ON a.kepada=b.nm_instansi WHERE b.kd_instansi='$kd_instansi' AND status='B'");
											$num=@mysql_num_rows($query);
											$no=1;
											while($r=@mysql_fetch_array($query)){
										?>
                                            <tr>
                                               
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $r['tujuan']; ?></td>
												<td><?php echo $r['no_ts']; ?></td>
												<td><?php echo $r['isi']; ?></td>
                                                <td><?php echo $r['tanggal']; ?></td>
												
												<th>
													<!--button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Setuju" onclick="window.location='modul/mod_kegadmin/konfirmasi_setuju.php?no_ts=<?php echo $r['no_ts']; ?>"><span>Setuju</span></button-->

                                                    <a href="modul/mod_kegadmin1/konfirmasi_setuju.php?no_ts=<?php echo $r['no_ts']; ?>" class="btn btn-success"><span>Setuju</span></a>

                                                    <a href="modul/mod_kegadmin1/konfirmasi_tidak.php?no_ts=<?php echo $r['no_ts']; ?>" class="btn btn-danger"><span>Tidak</span></a>

                                                    <a href="#" class="btn btn-primary"><span>Lihat TS</span></a>

													<!--button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Tidak" onclick="window.location='<?php echo $aksi."?module=delete&&id=".$r['no_ts']; ?>'"><i class="">Tidak</i></button-->
												</th>
                                            </tr>
										<?php
										$no++;
										}
										?>
                                        </tbody>
                                    </table>
									</div>
									<?php
							$jmldata=@mysql_num_rows(mysql_query("SELECT * FROM ts"));
							$jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
							$linkHalaman = $p->navHalaman($_GET['halaman'], $jmlhalaman); 
							echo "$linkHalaman";
							
							?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;

case "update":
$no_ts = $_GET['id'];
$query=mysql_query("SELECT * FROM ts WHERE ts.no_ts = '$no_ts'");
$r=mysql_fetch_array($query);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>

                     
                    </div>
                    <div class="clearfix"></div>


                   <!--div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Update <small><?php echo $r['no_ts']; ?></small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $aksi."?module=update"; ?>">
									<input type="hidden" name="no_ts" value="<?php echo $r['no_ts']; ?>">
									<input type="hidden" id="last-name" name="id_staf" value="<?php echo $username ?>" class="form-control col-md-7 col-xs-12">
									<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nomor Surat
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="t1" class="form-control" value="<?php echo $r['no_ts'] ?>">
                                                <span class="fa fa-sort-numeric-asc form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
										
										 <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kepada 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="select2_single form-control" tabindex="-2" name="t2" id="selec">
													
													<?php
													$query_provinsi=mysql_query("SELECT * FROM ts WHERE kepada='$kepada'");
													while($rpro=mysql_fetch_array($query_provinsi)){
													?>
													<option disabled selected>Pilih</option>
                                                        <option value="A"> Bupati Kabupaten Tanah Laut </option>
                                                        <option value="B"> Sekretariat Daerah </option>
													<?php
													}
													?>
												
												</select>
											</div>
                                        </div>
										
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Dari
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="kota" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['pengirim'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="date" class="form-control" name="tanggal" value="<?php echo $r['tanggal'] ?>">
                                                <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Perihal
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t4" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['perihal'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Keterangan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" name="t8" value="">
                                            <input type="text" class="form-control" name="keter2" value="">
                                            <input type="text" class="form-control" name="keter3" value="">
                                            <input type="text" class="form-control" name="t8" value="">
                                            <input type="text" class="form-control" name="keter2" value="">
                                            <input type="text" class="form-control" name="keter3" value="">
                                            <input type="text" class="form-control" name="t8" value="">
                                            <input type="text" class="form-control" name="keter2" value="">
                                            <input type="text" class="form-control" name="keter3" value="">
                                            <input type="text" class="form-control" name="keter3" value="">
                                            </div>
                                               
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tujuan 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="t3" rows="3"><?php echo $r['tujuan']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tempat Tujuan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="kota" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['tempat_tujuan'] ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Isi 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="t3" rows="3"><?php echo $r['isi']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kesimpulan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="t3" rows="3"><?php echo $r['kesimpulan']?></textarea>
                                            </div>
                                        </div>
                                        
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Saran dan Tindakan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="t3" rows="3"><?php echo $r['saran_tindakan']?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Penutup
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control" name="t3" rows="3"><?php echo $r['penutup']?></textarea>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div-->
<?php
break;
}
?>