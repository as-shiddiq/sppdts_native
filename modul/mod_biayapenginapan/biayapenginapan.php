<script>
	$(document).ready(function(){
		$("#form_add").css("display","none");
		$("#add").click(function(){
			$("#form_add").fadeToggle(1000);

		});
	});
</script>
<script type="text/javascript">
		 $(document).ready(function() {
		  $("#txtcari").keyup(function() {
		   var strcari = $("#txtcari").val();
		   if (strcari != "")
		   {
		   $("#tabel_awal").css("display", "none");

			$("#hasil").html("<img src='images/loader.gif'/>")
			$.ajax({
			 type:"post",
			 url:"modul/mod_biayapenginapan/cari.php",
			 data:"q="+ strcari,
			 success: function(data){
			 $("#hasil").css("display", "block");
			  $("#hasil").html(data);
			  
			 }
			});
		   }
		   else{
		   $("#hasil").css("display", "none");
		   $("#tabel_awal").css("display", "block");
		   }
		  });
			});
	</script>
<?php	
$aksi="modul/mod_biayapenginapan/aksi_biayapenginapan.php";
$judul="Biaya Penginapan Perjalanan Dinas Dalam Negeri";
switch(isset($_GET['act'])){
	default:
	$p      = new Paging;
    $batas  = 10;
    $posisi = $p->cariPosisi($batas);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>
					</div>
                    <div class="clearfix"></div>


                    <div class="row">
                                <div class="x_content">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<button type="button" class="btn btn-default btn-sm" id="add"><i class="fa fa-plus-circle"></i> Tambah Uang Harian</button>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" required="required" class="form-control" placeholder="Search" id="txtcari">
                                    </div>
									<div class="row" id="form_add">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Tambah Uang Harian</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form action="<?php echo $aksi."?module=add" ?>" method="post" data-parsley-validate class="form-horizontal form-label-left">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="t1" type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Satuan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t2" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pejabat Negara / Pejabat Eselon I
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t3" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pejabat Negara / Pejabat Eselon II
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t4" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pejabat Eselon III / Golongan IV
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t5" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pejabat Eselon IV / Golongan III
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t6" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Golongan I/II
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t7" required="required" class="form-control col-md-7 col-xs-12">
                                            </div>
                                        </div>
										
										
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
									<div id="hasil"></div>
									<div id="tabel_awal">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">No</th>
                                                <th rowspan="2">PROVINSI</th>
                                                <th rowspan="2">SATUAN</th>
												<th colspan="5">TARIF HOTEL</th>
												<th rowspan="2"></th>
                                            </tr>
											<tr>
												<th width="80px">PEJABAT NEGARA/PEJABAT ESELON I</th>
												<th width="80px">PEJABAT NEGARA/PEJABAT ESELON II</th>
												<th width="80px">PEJABAT ESELON III/GOLONGAN IV</th>
												<th width="80px">PEJABAT ESELON IV/GOLONGAN III</th>
												<th width="80px">GOLONGAN I/II</th>
											</tr>
                                        </thead>
                                        <tbody>
										<?php
											$query=mysql_query("SELECT * FROM tbl_biayapenginapan, tbl_provinsi WHERE tbl_biayapenginapan.id_biapeng=tbl_provinsi.id_provinsi ORDER BY tbl_biayapenginapan.id_biapeng ASC LIMIT $posisi,$batas");
											$num=mysql_num_rows($query);
											$no=1;
											while($r=mysql_fetch_array($query)){
										?>
                                            <tr>
                                               
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $r['nama_provinsi']; ?></td>
												<td><?php echo $r['satuan']; ?></td>
												<td><?php echo $r['pnpeI']; ?></td>
                                                <td><?php echo $r['pnpeII']; ?></td>
												<td><?php echo $r['pnpeIIIgolIV']; ?></td>
                                                <td><?php echo $r['peIVgolIII']; ?></td>
												<td><?php echo $r['golongan']; ?></td>
												
												<th>
													<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Update" onclick="window.location='media.php?module=biayapenginapan&&act=update&&id_uang=<?php echo $r['id_biapeng']; ?>'"><i class="fa fa-retweet"></i> Update</button>
													<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" onclick="window.location='<?php echo $aksi."?module=delete&&idpeng=".$r['id_biapeng']; ?>'"><i class="fa fa-trash"></i> Delete</button>
												</th>
                                            </tr>
										<?php
										$no++;
										}
										?>
                                        </tbody>
                                    </table>
									</div>
									<?php
							$jmldata=mysql_num_rows(mysql_query("SELECT * FROM tbl_biayapenginapan"));
							$jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
							$linkHalaman = $p->navHalaman($_GET['halaman'], $jmlhalaman); 
							echo "$linkHalaman";
							
							?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;

case "update":
$id_uang=$_GET['id_uang'];
$query=mysql_query("SELECT * FROM tbl_biayapenginapan WHERE id_biapeng='$id_uang'");
$r=mysql_fetch_array($query);
?>
<div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo $judul; ?></h3>
                        </div>

                     
                    </div>
                    <div class="clearfix"></div>


                   <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Update <small><?php echo $r['provinsi']; ?></small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $aksi."?module=update"; ?>">
									<input type="hidden" name="id" value="<?php echo $r['id_biapeng']; ?>">
                                         <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi 
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="t1" type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['provinsi']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Satuan
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t2" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['satuan']; ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pejabat Negara / Pejabat Eselon I
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t3" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['pnpeI']; ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pejabat Negara / Pejabat Eselon II
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t4" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['pnpeII']; ?>">
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pejabat Eselon III / Golongan IV
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t5" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['pnpeIIIgolIV']; ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pejabat Eselon IV / Golongan III
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t6" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['peIVgolIII']; ?>">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Golongan I/II
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="last-name" name="t7" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $r['golongan']; ?>">
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
break;
}
?>