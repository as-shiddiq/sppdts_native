<?php
	include ("../../config/fungsi_indotgl.php");
	include ("../../config/koneksi.php");
	$id_spt=$_GET['id_spt'];
	$tgl_sek=$_POST['tgl_sekarang'];
	?>
<!doctype html>
<html>
	<head>
		
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
			.check{
				width:15px;
				height:15px;
			}
			.logo{
				width:100px;
				height:100px;
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;
		
		}
		.kop .judul{
			text-align:center;
			
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}
		
	
			
			.garis{
				line-height:1.5px;
			}
			.garis .line1{
				border:1px solid #000;
			}
			.garis .line2{
				border:0.5px solid #000;
			}
			.heading{
				margin:5px 0px 10px 0px;
				text-align:center;
				font-size:13px;
			}
			.b{
				font-style:bold;
			}
			.tbl_lap1{
				margin-bottom:10px;
			}
			.tbl_lap1 td{
				font-size:11px;
			}
			.tbl_lap1 .cel1{
				width:200px;
			}
			.tbl_lap1 .cel2{
				width:0px;
			}
			.tbl_lap1 .cel3{
				width:450px;
			}
			.baris td{
				padding:4px 10px;
			}
			
			.tbl_lap2{
				border-collapse:collapse;
			}
			.tbl_lap2 td{
				font-size:11px;
			}
			.tbl_lap2 .judul td{
				padding:3px 10px;
				text-align:center;
			}
			.tbl_lap2 .cel1{
				width:0px;
			}
			.tbl_lap2 .cel2{
				width:180px;
			}
			.tbl_lap2 .cel3{
				width:50px;
			}
			.tbl_lap2 .cel4{
				width:50px;
			}
			.tbl_lap2 .cel5{
				width:250px;
			}
			
			.tbl_lap3 .left{
				width:375px;
				text-align:center;
				font-size:13px;
			}
			.tbl_lap3 .right{
				width:375px;
				text-align:center;
				font-size:13px;
			}
			.catatan{
				font-size:10px;
				margin:10px 0px;
				line-height:20px;
			}
			.cc{
				text-align:center;
			}
		</style>
	</head>
	<body>
	<?php
		$queryForm=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_pegawai, tbl_provinsi WHERE tbl_kegiatan.pergi=tbl_provinsi.id_provinsi AND tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
		$rF=mysql_fetch_array($queryForm);
	?>
		<div class="page">
		<table class="kop">
				<tr>
					<td class="header">
						<img src="../../images/logo_perhunungan.png" class="logo"> 
					</td>
					<td  class="judul">
						<H3>SISTEM PERJALANAN DINAS PADA</H3>
						<h2>KANTOR DINAS MARTECH</h2>
                        
						<table>
							<tr>
								<td class="cel1">Jalan : <br>
								Medan, Indonesia
								</td>
								<td class="cel2">Telepon : 061-xxxx<br>
								061 - xxxx xxxx Ext xxxx<br>
								061 - xxxx xxxx Ext xxxx
								</td>
								<td class="cel3">Fax : 061-xxxxxxxx<br>
								Email : perjalanandinas@gmail.com
								</td>
							</tr>
						
						</table>
		
					</td>
				</tr>
			</table>
		<div class="garis">
			<hr class="line1"/>
			<hr class="line2"/>
		</div>
		<div class="heading">
			<b><u>BUKTI PENYERAHAN BERKAS</u></b>
			
		</div>
		
		<table class="tbl_lap1">
			<tr class="baris">
				<td class="cel1">NAMA</td><td class="cel2">:</td><td class="cel3"> <?php echo $rF['nama'] ?></td>
			</tr>
			<tr class="baris">
				<td>NIP</td><td>:</td><td class="cel3"> <?php echo $rF['nip'] ?></td>
			</tr>
			<tr class="baris">
				<td>JABATAN</td><td>:</td><td class="cel3"> <?php echo $rF['jabatan'] ?></td>
			</tr>
			<tr class="baris">
				<td>UNIT KERJA</td><td>:</td><td class="cel3"> <?php echo $rF['instansi'] ?></td>
			</tr>
			<tr class="baris">
				<td>BULAN</td><td>:</td><td class="cel3"> <?php echo getBln($rF['tgl_spt']) ?></td>
			</tr>
			<tr class="baris">
				<td>NO. &amp; TANGGAL SPT</td><td>:</td><td class="cel3"> <?php echo $rF['no_kegiatan'] ?> / <?php echo tgl_indo($rF['tgl_spt']) ?></td>
			</tr>
			<tr class="baris">
				<td>TUJUAN</td><td>:</td><td class="cel3"> <?php echo $rF['kota'] ?></td>
			</tr>
			<tr class="baris">
				<td>WAKTU PELAKSANAAN</td><td>:</td><td class="cel3"> <?php echo tgl_indo($rF['tglmulai']) ?> s/d <?php echo tgl_indo($rF['tglselesai']) ?></td>
			</tr>
			<tr class="baris">
				<td>KEPERLUAN</td><td>:</td><td class="cel3"> <?php echo $rF['keperluan'] ?></td>
			</tr>
		</table>
		<?php
			$query=mysql_query("SELECT * FROM tbl_spt WHERE id_spt=$id_spt");
			$r=mysql_fetch_array($query);
		?>
		<table class="tbl_lap2" border="1">
			<tr class="judul"  valign="middle">
				<td class="cel1">NO.</td><td class="cel2">BERKAS</td><td class="cel3">ADA</td><td class="cel4">TIDAK<br>ADA</td><td class="cel5">KETERANGAN</td>
			</tr>
			<tr class="baris">
				<td>1</td><td>RINCIAN PERJALANAN DINAS</td>
				<?php
				if($r['buk_rpd']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket1']; ?></td>
			</tr>
			<tr class="baris">
				<td>2</td><td>SPPD</td>
				<?php
				if($r['buk_sppd']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket2']; ?></td>
			</tr>
			<tr class="baris">
				<td>3</td><td>SPT</td>
				<?php
				if($r['buk_spt']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket3']; ?></td>
			</tr>
			<tr class="baris">
				<td>4</td><td>TIKET</td>
				<?php
				if($r['buk_tiket']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket4']; ?></td>
			</tr>
			<tr class="baris">
				<td>5</td><td>AIRPORT TAX</td>
				<?php
				if($r['buk_tax']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket5']; ?></td>
			</tr>
			<tr class="baris">
				<td>6</td><td>BOARDING PASS</td>
				<?php
				if($r['buk_pass']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket6']; ?></td>
			</tr>
			<tr class="baris">
				<td>7</td><td>PENGINAPAN</td>
				<?php
				if($r['buk_penginapan']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket7']; ?></td>
			</tr>
			<tr class="baris">
				<td>8</td><td>DAFTAR PENGELUARAN RIIL</td>
				<?php
				if($r['buk_pengriil']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket8']; ?></td>
			</tr>
			<tr class="baris">
				<td>9</td><td>SURAT PERNYATAAN</td>
				<?php
				if($r['buk_pernyataan']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket9']; ?></td>
			</tr>
			<tr class="baris">
				<td>10</td><td>KWITANSI / SURAT KETERANGAN SEWA<br>KENDARAAN (BERMATERAI)</td>
				<?php
				if($r['buk_kwitansi']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket10']; ?></td>
			</tr>
			<tr class="baris">
				<td>11</td><td>RINCIAN PERJALANAN DINAS</td>
				<?php
				if($r['buk_lap']=='Y'){
				?>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
					<td></td>
				<?php
				}
				else{
				?>
					<td></td>
					<td class='cc'><img src='../../images/check.png' class='check'></td>
				<?php
				}
				?>
				<td class="cel5"><?php echo $r['ket11']; ?></td>
			</tr>
		</table>
		<i class="catatan">Bukti penyerahan berkas pertanggung jawaban dinas ini <b>Wajib Diketahui dan Ditandatangani</b> oleh pegawai yang menyerahkan<br>
		berkas dan tim verifikasi.</i>
		<?php
			$log=$_GET['id_log'];
			$ttd1=mysql_query("SELECT * FROM tbl_pegawai WHERE id_pegawai=$log");
			$rtd1=mysql_fetch_array($ttd1);
		?>
		<table class="tbl_lap3">
			<tr>
				<td class="left">
				<br>
					YANG MENERIMA,<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
										<br>
					<br>
					<u><?php echo $rtd1['nama']; ?></u><br>
					NIP. <?php echo $rtd1['nip']; ?><br>
				</td>
				<td class="right">	
					<br>
					Deliserdang, <?php 
			
					echo tgl_indo($tgl_sek);
					?><br>
					YANG BEPERGIAN<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<u><?php echo $rF['nama'] ?></u><br>
					NIM : <?php echo $rF['nip'] ?><br>
				</td>
			</tr>
		</table>
		<i class="catatan">Tim Verifikasi <b>Menolak</b> apabila masih terdapat ketidaksamaan / tidak lengkap terhadap bukti-bukti Pendukung Perjalanan Dinas ini.</i>
		</div>
	</body>
</html>