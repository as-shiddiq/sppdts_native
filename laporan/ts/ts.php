<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	?>
<!doctype html>
<html>
	<head>
		<title></title>
		<style>
		html{
			font-family: sans-serif;
		}
    .tbl_field td{
        border-bottom: 2px solid #999;
    }
		.logo{
				width:80px;
				height:80px;
				margin-top: 10px
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;

		}
		.kop .judul{
			text-align:center;

		}
		.judul p{
			margin: 0;
			padding: 0
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}

		.garis{
			line-height:1.5px;
		}
		.garis .line1{
			border:1px solid #000;
		}
		.garis .line2{
			border:0.5px solid #000;
		}

		.baris td{
			padding:10px 8px;
		}
    .nomor{
      text-align: center;
    }
		.judul h3{
			padding: 0;
      margin: 0;
			font-size: 22px
  }
    .nomor p{
      padding: 0;
      margin: 0
    }
    .nomor h3{
      padding: 0;
      margin: 0
    }
    table{
      width: 100%;
      border-spacing: 0;
    }
    table tr td{
      vertical-align: top;
			padding-bottom: 10px
    }
    .width100{
      width: 100px;
    }
    .width150{
      width: 150px
    }
    .width10{
      width: 10px
    }
    .pading10 tr td{
      padding: 10px
    }
    .paddingbottom10 tr td{
      padding-bottom: 10px
    }
    .paddingbottom20 tr td{
      padding-bottom: 20px
    }
    .tablesub tr td{
      padding-bottom: 5px
    }
		.ttd tr td{
			width: 30%
		}
		.center{
			text-align: center;
		}
		</style>
	</head>
	<body>
		<table class="kop">
      <?php
    		$no_ts=$_GET['no_ts'];
        $sql="SELECT * FROM ts a LEFT JOIN instansi b ON a.kd_instansi=b.kd_instansi
              WHERE a.no_ts='$no_ts'";
    		$query=mysql_query($sql) or die($sql);
        $f=mysql_fetch_array($query);
  		?>
			<tr>
				<td width="80px">
					<img src="../../images/logo1.jpg" class="logo">
				</td>
				<td  class="judul">
					<h3>
            PEMERINTAH KABUPATEN TANAH LAUT <br>
            <?=strtoupper($f['nm_instansi'])?>
          </h3>
          <p><?=$f['alamat']?></p>
				</td>
			</tr>
		</table>

		<div class="garis">
			<hr class="line1"/>
			<hr class="line2"/>
		</div>

    <div class="nomor">
      <u>
        <h3>
          TELAAHAN STAF
        </h3>
      </u>
      <p>
        Nomor : <?php echo $f['no_ts']; ?>
      </p>
      <br>
    </div>
    <table class="padding10">
      <tr>
        <td class="width100">Kepada</td>
        <td class="width10">:</td>
        <td><?=$f['kepada']?></td>
      </tr>
      <tr>
        <td>Dari</td>
        <td>:</td>
        <td>Kepala <?=$f['nm_instansi']?></td>
      </tr>
      <tr>
        <td>Nomor</td>
        <td>:</td>
        <td><?=$f['kepada']?></td>
      </tr>
      <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td><?=tgl_indo($f['tanggal'])?></td>
      </tr>
      <tr>
        <td>Perihal</td>
        <td>:</td>
        <td><?=$f['perihal']?></td>
      </tr>


    </table>
    <hr style="margin:0;border:0;border-top:2px double #000">
		<!--?php
			$no_ts=$r['no_ts'];
		?-->
    <?php
      $no_ts=$_GET['no_ts'];
      $sql="SELECT * FROM detail_ts a LEFT JOIN dasar b ON a.kd_dasar=b.kd_dasar
            WHERE a.no_ts='$no_ts'";
      $query=mysql_query($sql) or die($sql);
    ?>
		<table  class="tbl_lap2 padding10 paddingbottom10">
	    <tr>
				<td class="width150">Dasar</td>
				<td class="width10"> : </td>
				<td>
          <table class="tablesub">
            <?php
              $no=1;
              while ($dasar = mysql_fetch_array($query)) {
                ?>
                <tr>
                  <td><?=$no++?>.</td>
                  <td>
                    <?=$dasar['ket_dasar']?>
                  </td>
                </tr>
                <?php
              }
             ?>
          </table>
        </td>
			</tr>
      <tr>
        <td>Isi</td>
        <td>:</td>
        <td><?=$f['isi']?></td>
      </tr>
      <tr>
        <td>Kesimpulan</td>
        <td>:</td>
        <td><?=$f['kesimpulan']?></td>
      </tr>
      <tr>
        <td>Saran & Tindakan</td>
        <td>:</td>
        <td><?=$f['saran_tindakan']?></td>
      </tr>
      <tr>
        <td>Penutup</td>
        <td>:</td>
        <td><?=$f['penutup']?></td>
      </tr>
		</table>
		<br>
		<table class="ttd">
			<tr>
        <td>
          &nbsp;
        </td>
        <td class="center width100" >
					<br>
          Pelaihari, <?php echo tgl_indo(date('Y-m-d')); ?>
					<br>
					<?=strtoupper('Kepala '.$f['nm_instansi'])?>
					<br>
					<br>
					<br>
					<br>
          <u><?php echo $f['kepala_dinas']; ?></u><br>
					NIP : <?php echo $f['NIP']; ?>
        </td>
			</tr>
		</table>
	</body>
</html>
