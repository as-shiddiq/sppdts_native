<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	$id_spt=$_POST['id_spt'];
	//$id_pegawai=$_POST['id_pegawai'];
	$tgl_sek=$_POST['tgl_sekarang'];
    $qq=mysql_query("SELECT * FROM tbl_spt WHERE id_spt='$id_spt'");
    $rr=mysql_fetch_array($qq);
    $id_pegawai=$rr['id_pegawai'];
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		.logo{
				width:100px;
				height:100px;
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;
		
		}
		.kop .judul{
			text-align:center;
			
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}
		
	
			
			.garis{
				line-height:1.5px;
			}
			.garis .line1{
				border:1px solid #000;
			}
			.garis .line2{
				border:0.5px solid #000;
			}
		.tbl_lap1{
			margin:20px 0px;
		}
		.tbl_lap1 .cel1{
			width:180px;
		}
		.tbl_lap1 .cel2{
			width:200px;
		}
		.tbl_lap1 .cel3{
			width:290px;
		}
		.judul{
			margin:20px 0px;
			text-align:center;
		}
		.tbl_lap2{
			border-collapse:collapse;
			border:1px solid #000;
		}
		.tbl_lap2 .cel1{
			width:20px;
		}
		.tbl_lap2 .cel2{
			width:200px;
		}
		.tbl_lap2 .cel3{
			width:400px;
		}
		.tbl_lap2 td{
			padding:5px 2px;
		}
		.space{
			padding:10px;
		}
		.tbl_lap3 .cel1{
			width:300px;
		}
		.tbl_lap3 .cel2{
			width:100px;
		}
		.tbl_lap3 .cel3{
			width:300px;
		}
		</style>
	</head>
	<body>
		<div class="page">
			<div class="kops">
			<table class="kop">
				<tr>
					<td class="header">
						<img src="../../images/logo_perhunungan.png" class="logo"> 
					</td>
					<td  class="judul">
						<H3>SISTEM PERJALANAN DINAS PADA</H3>
						<h2>KANTOR DINAS MARTECH</h2>
                        
						<table>
							<tr>
								<td class="cel1">Jalan : <br>
								Medan, Indonesia
								</td>
								<td class="cel2">Telepon : 061-xxxx<br>
								061 - xxxx xxxx Ext xxxx<br>
								061 - xxxx xxxx Ext xxxx
								</td>
								<td class="cel3">Fax : 061-xxxxxxxx<br>
								Email : perjalanandinas@gmail.com
								</td>
							</tr>
						
						</table>
		
					</td>
				</tr>
			</table>
		</div>
		<div class="garis">
			<hr class="line1"/>
			<hr class="line2"/>
		</div>
			<?php
			$query=mysql_query("SELECT * FROM tbl_pembatalan, tbl_spt, tbl_kegiatan WHERE tbl_pembatalan.id_spt=tbl_spt.id_spt AND tbl_spt.id_spt='$id_spt' AND tbl_kegiatan.id_kegiatan=tbl_spt.id_kegiatan");
			$r=mysql_fetch_array($query);
			?>
			<div class="judul">SURAT PERYATAAN PEMBATALAN TUGAS PERJALANAN DINAS JABATAN<br>Nomor <b><?php echo $r['no_su']; ?></b></div>
			<table class="tbl_lap2">
			<?php
			$query1=mysql_query("SELECT * FROM tbl_pembatalan, tbl_pegawai WHERE tbl_pembatalan.id_pempernyataan=tbl_pegawai.id_pegawai ORDER BY tbl_pembatalan.id_pembatalan DESC");
			$r1=mysql_fetch_array($query1);
			?>
				<tr>
					<td colspan="3">Yang bertanda tanggan dibawah ini:</td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Nama</td><td class="cel3">: <?php echo $r1['nama']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">NIP</td><td class="cel3">: <?php echo $r1['nip']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Jabatan</td><td class="cel3">: <?php echo $r1['jabatan']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Satker</td><td class="cel3">: <?php echo $r1['instansi']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Kementrian/Lembaga</td><td class="cel3">: Kementerian Perhubungan</td>
				</tr>
				<tr>
					<td colspan="3">menyatakan dengan sesunggunhnya, bahwa Perjalanan Dinas Jabatan atas nama:</td>
				</tr>
				<?php
			$query2=mysql_query("SELECT * FROM tbl_pegawai WHERE tbl_pegawai.id_pegawai='$id_pegawai'");
			$r2=mysql_fetch_array($query2);
			?>
				<tr>
					<td class="cel1"></td><td class="cel2">Nama</td><td class="cel3">: <?php echo $r2['nama']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">NIP</td><td class="cel3">: <?php echo $r2['nip']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Jabatan</td><td class="cel3">: <?php echo $r2['jabatan']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Satker</td><td class="cel3">: <?php echo $r2['instansi']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Kementrian/Lembaga</td><td class="cel3">: Kementerian Lembaga</td>
				</tr>
				<?php
					$query3=mysql_query("SELECT * FROM tbl_pembatalan, tbl_spt WHERE tbl_pembatalan.id_spt=tbl_spt.id_spt AND tbl_spt.id_spt='$id_spt'");
					$r3=mysql_fetch_array($query3);
				?>
				<tr>
					<td colspan="3">dibatakan atau tidak dapat dilaksanakan desebabkan adanya keperluan dinas lainya yang sangat mendesak/penting dan tidak dapat ditunda yaitu <b><?php echo $r3['penyebab']; ?></b><br> 
					<br>
					<br>
					Sehubungan dengan pembatalan tersebut, pelaksanaan perjalanan dinas tidak dapat digantikan oleh pejabat/pegawai negeri lain.
					.<br><br>
					Demikian surat pernyataan ini dibuat dengan sebenarnya dan apabila dikemudia hari ternyata surat pernyataan ini tidak benar, saya bertanggun jawab penuh dan bersedia diproses sesuai dengan ketentuan hukum yang berlaku.
					</td>
				</tr>
			</table>
			<p class="space"></p>
			<table class="tbl_lap3" border="0px">
				<?php
				$query1=mysql_query("SELECT * FROM tbl_pembatalan, tbl_pegawai WHERE tbl_pembatalan.id_pempernyataan=tbl_pegawai.id_pegawai ORDER BY tbl_pembatalan.id_pembatalan DESC");
			$r1=mysql_fetch_array($query1);
				?>
				<tr>
					<td class="cel1"></td>
						<td class="cel2"></td><td class="cel3">
					Medan, <?php echo tgl_indo($tgl_sek); ?><br>
					<br>
					Yang Membuat Pernyataan<br>
					<br>
					<br>
					<br>
					<br>
					<?php echo $r1['nama']; ?><br>
					NIP. <?php echo $r1['nip']; ?>
					</td>
				</tr>
			</table>
			
		</div>
	</body>
</html>