<?php
	include ("../../config/koneksi.php");
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		.judul{
			margin:20px 0px;
			text-align:center;
			font-size:16px;
		}
		.tbl_lap1{
			border-collapse:collapse;
		}
		.tbl_lap1 td{
			padding:5px;
		}
		.tbl_lap1 .cel1{
			width:20px;
		}
		.tbl_lap1 .cel2{
			width:250px;
		}
		.tbl_lap1 .cel3{
			width:180px;
		}
		.tbl_lap1 .cel4{
			width:230px;
		}
		.tbl_lap1 .cel5{
			width:100px;
		}
		.tbl_lap1 .cel6{
			width:150px;
		}
		.kete .cel1{
			width:10px;
		}
		.kete .cel2{
			width:900px;
		}
		.kete td{
			padding:5px;
		}
		.tbl_ttd .cel1{
			width:700px;
		}
		</style>
	</head>
	<body>
		<div class="pot">
			<div class="judul">Monitoring Penerbitan Surat Tugas dalam Pelaksanaan PDJ untuk Bulan................. Tahun ...............</div>
			<table border="1px" class="tbl_lap1">
				<tr>
					<td rowspan="2" class="cel1" align="center" valign="middle">No</td><td rowspan="2" class="cel2" align="center" valign="middle">Nama Pelaksanaan SPD/NIP</td><td colspan="2" class="cel3" align="center" valign="middle">Surat Tugas</td><td colspan="2" class="cel4" align="center" valign="middle">Tanggal Pelaksanaan PDJ</td><td rowspan="2" class="cel5" align="center" valign="middle">Tujuan</td><td rowspan="2" class="cel6" align="center" valign="middle">Keterangan *)</td>
				</tr>
				<tr>
					<td align="center">Nomor</td><td align="center">Tanggal</td><td align="center">Mulai</td><td align="center">Selesai</td>
				</tr>
				<tr>
					<td align="center">(1)</td><td align="center">(2)</td><td align="center">(3)</td><td align="center">(4)</td><td align="center">(5)</td><td align="center">(6)</td><td>(7)</td><td align="center">(8)</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
				</tr>
			</table>
			
			<table class="kete">
				<tr>
					<td colspan="2"><b>Keterangan *)</b></td>
				</tr>
				<tr>
					<td class="cel1">1</td><td class="cel2">Diisi dengan jenis kegiatan perjalanan dinas, misalnya dalam rangka narasumber/rapat/rapat pimpinan/(rapim)/rapim terbatas/rakor/monev/survei, dsb.</td>
				</tr>
				<tr>
					<td>2</td><td>Diisi "dibatalkan" apabila terdapat pembatalan pelaksanaan perjalanan dinas.</td>
				</tr>
				<tr>
					<td>3</td><td>Terdapat pelaksanaan tugas awal yang sebelum selesai tetapi dilanjutkan pelaksanaan tugas lain.</td>
				</tr>
				<tr>
					<td>4</td><td>Dapat diisi dengan keterangan lainya</td>
				</tr>
			</table>
			<hr>
			<table class="tbl_ttd" border="0px">
				<tr>
					<td class="cel1"></td><td class="cel2">DIREKTUR KANTOR DINAS MARTECH
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					Martinus Sarumaha
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>