<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	?>
<!doctype html>
<html>
	<head>
		<title></title>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		html{
			font-family: sans-serif;
		}
		body{
			padding: 10px 30px !important;
		}
    .tbl_field td{
        border-bottom: 2px solid #999;
    }
		.logo{
				width:80px;
				height:80px;
				margin-top: 10px
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;

		}
		.kop .judul{
			text-align:center;

		}
		.judul p{
			margin: 0;
			padding: 0
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}

		.garis{
			line-height:1.5px;
		}
		.garis .line1{
			border:1px solid #000;
		}
		.garis .line2{
			border:0.5px solid #000;
		}

		.baris td{
			padding:10px 8px;
		}
    .nomor{
      text-align: center;
    }
		.judul h3{
			padding: 0;
      margin: 0;
			font-size: 22px
  }
    .nomor p{
      padding: 0;
      margin: 0
    }
    .nomor h3{
      padding: 0;
      margin: 0
    }
    table{
      width: 100%;
      border-spacing: 0;
    }
    table tr td{
      vertical-align: top;
			padding-bottom: 10px
    }
    .width100{
      width: 100px;
    }
    .width150{
      width: 150px
    }
    .width10{
      width: 10px
    }
    .pading10 tr td{
      padding: 10px
    }
    .paddingbottom10 tr td{
      padding-bottom: 10px
    }
    .paddingbottom20 tr td{
      padding-bottom: 20px
    }
    .tablesub tr td{
      padding-bottom: 5px
    }
		.ttd tr td{
			width: 30%
		}
		.center{
			text-align: center;
		}
		.border td{
			border: 1px solid #000
		}
		</style>
	</head>
	<body>
		<div class="page">
		<?php
		$no_ts=$_GET['no_ts'];
		$query=mysql_query("SELECT *,a.tanggal as tanggal_ts FROM ts a LEFT JOIN spt b ON a.no_ts=b.no_ts
												LEFT JOIN instansi c ON a.kd_instansi=c.kd_instansi WHERE a.no_ts='$no_ts'") or die(mysql_error());
		$r=mysql_fetch_array($query) ;
		extract($r);
		if($kepada!='Sekretariat Daerah'){
				?>
				<br>
				<table class="kop">
					<tr>
						<td  class="judul">
							<h3>
								<img src="../../images/logo_garuda.png" class="logo">
								<br>
								<br>
		            BUPATI TANAH LAUT <br>
		          </h3>
						</td>
					</tr>
				</table>
				<?php
		}
		else{
			?>
			<table class="kop">
				<tr>
					<td width="80px">
						<img src="../../images/logo1.jpg" class="logo">
					</td>
					<td  class="judul">
						<?php
							$sql="SELECT * from instansi WHERE nm_instansi ='$kepada'";
							$q=mysql_query($sql) or die(mysql_error());
							$f=mysql_fetch_array($q);
						?>
						<h3>
	            PEMERINTAH KABUPATEN TANAH LAUT <br>
	            <?=strtoupper($f['nm_instansi'])?>
	          </h3>
	          <p><?=$f['alamat']?></p>
					</td>
				</tr>
			</table>
			<?php
		}
		?>
		<div class="garis">
			<hr class="line1"/>
			<hr class="line2"/>
		</div>
		<div class="heading center">
			<br>

			<u>SURAT PERINTAH TUGAS</u><br>
			Nomor : <?php echo $no_spt ?>
		</div>
		<!--?php
			$no_ts=$r['no_ts'];
		?-->
		<table  class="tbl_lap2">
		<tr class="baris">
				<td class="cel1" width="100px">Dasar</td>
				<td  class="cel2" width="10px"> : </td>
				<td  class="cel3">Telaahan Staf Nomor <?=$no_ts?> Tanggal <?=tgl_indO($tanggal_ts)?>, Perihal <?php echo $perihal; ?></td>
			</tr>
		</table>

		<table class="tbl_lap1">
			<tr>
				<td colspan="4">
					<br>
				<b>DIPERINTAHKAN KEPADA :</b></td>
			</tr>
			<tr class="tbl_field border">
				<td class="cel1 center"><b>No.</b> </td>
				<td class="cel2 center" align="left"><b> NAMA / NIP</b> </td>
				<td class="cel3" align="center"><b>PANGKAT/GOL</b> </td>
				<td class="cel4" align="center"><b>JABATAN</b> </td>
			</tr>
			<?php
			$sql= "SELECT * FROM pegawai_disarankan a LEFT JOIN pegawai b ON a.nip=b.nip  where a.no_ts='$no_ts'";
			$data= mysql_query($sql) or die(mysql_error());
			$no=1;
			while($fr=mysql_fetch_array($data)){
			?>
			<tr class="baris border">
				<td class="center"><?php echo $no; ?>. </td>
				<td class="cel2"><u><b><?php echo $fr['nama']; ?></b></u><br>
				<?php echo $fr['nip']; ?>
				</td>
				<td  class="cel3" align="center"><?php echo $fr['pangkat']."<br>(".$fr['golongan'].")"; ?>
				</td>
				<td align="center" class="cel4"><?php echo $fr['jabatan']; ?>
				</td>
			</tr>
			<?php
			$no++;
			}
			?>
		</table>

		<table>
			<tr>
				<td colspan="4">
					<br>
					<b>Maksud Surat Perintah Tugas :</b>
				</td>
			</tr>
		<tr class="baris border">
				<td class="cel1" width="200px">Keperluan</td>
				<td  class="cel2" width="10px"> : </td>
				<td  class="cel3" width="500px"><?php echo $r['perihal']; ?></td>
			</tr>
			<tr class="baris border">
				<td>Tempat Tujuan</td>
				<td> : </td>
				<td align="left"><?php echo $r['tujuan']; ?></td>
			</tr>
			<tr class="baris border">
				<td>Mulai s.d Selesai</td>
				<td> : </td>
				<td align="left"><?php echo tgl_indo($tgl_mulai); ?> s.d <?php echo tgl_indo($tgl_selesai); ?></td>
			</tr>
			<tr class="baris border">
				<td>Transportasi</td>
				<td> : </td>
				<td align="left"><?php echo $transportasi ?></td>
			</tr>
			<tr class="baris border">
				<td colspan="3">Pembebanan Biaya SPT</td>
			</tr>
			<tr class="baris border">
				<td>Nomor Rekening</td>
				<td> : </td>
				<td align="left"><?php echo $no_rek ?></td>
			</tr>
			<tr class="baris border">
				<td>Organisasi</td>
				<td> : </td>
				<td align="left"><?php echo $nm_instansi ?></td>
			</tr>
			<tr class="baris border">
				<td>Program</td>
				<td> : </td>
				<td align="left"><?php echo $program; ?></td>
			</tr>
			<tr class="baris border">
				<td>Kegiatan</td>
				<td> : </td>
				<td align="left"><?php echo $kegiatan ?></td>
			</tr>
			<tr>
				<td colspan="4">
					<hr>
					<b>Demikian Surat Perintah Tugas ini dibuat untuk dilaksanakan sebagaimana mestinya dan setelah selesai menjalankan Surat Perintah Tugas ( SPT ) ini diharuskan menyampaikan hasil laporan kepada yang memberi tugas.</b>
				</td>
			</tr>
		</table>
		<?php if($kepada!='Sekretariat Daerah'){ ?>
		<table class="ttd">
			<tr>
				<td width="80%">&nbsp;</td>
				<td>
					<br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<u>Pada Tanggal : <?php echo tgl_indo(date('Y-m-d')); ?></u>
				</td>
			</tr>
			<tr>
        <td>
          &nbsp;
        </td>
        <td class="center" >
					BUPATI TANAH LAUT
					<br>
					<br>
					<br>
					<br>
          <u>H. BAMBANG ALAMSYAH</u><br>
        </td>
			</tr>
		</table>

		<?php } else {
			$sql="SELECT * from instansi WHERE nm_instansi ='$kepada'";
			$q=mysql_query($sql) or die(mysql_error());
			$f=mysql_fetch_array($q);
			?>
			<table class="ttd">
				<tr>
					<td width="80%">&nbsp;</td>
					<td>
						<br>

						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Pada Tanggal : <?php echo tgl_indo(date('Y-m-d')); ?>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
					<td class="center" >
						SEKRETARIS DAERAH
						<br>
						<br>
						<br>
						<br>
						<u><?php echo $f['kepala_dinas']; ?></u><br>
						NIP : <?php echo $f['NIP']; ?>
					</td>
				</tr>
			</table>
		<?php } ?>

		</div>
	</body>
</html>
