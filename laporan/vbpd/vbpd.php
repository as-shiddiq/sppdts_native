<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	include ("../../config/fungsi_rupiah.php");
	$id_spt=$_GET['id_spt'];
	$tgl_sek=$_POST['tgl_sekarang'];
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		.logo{
				width:100px;
				height:100px;
			}
				.logo{
				width:100px;
				height:100px;
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;
		
		}
		.kop .judul{
			text-align:center;
			
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}
		.garis{
			line-height:1.5px;
		}
		.garis .line1{
			border:1px solid #000;
		}
		.garis .line2{
			border:0.5px solid #000;
		}
		.heading{
			
			text-align:center;
			font-size:13px;
		}
		.b{
			font-style:bold;
		}
		.tbl_lap1{
			margin-bottom:50px;
		}
		.tbl_lap1 td{
			font-size:12px;
		}
		.tbl_lap1 .cel1{
			width:200px;
		}
		.tbl_lap1 .cel2{
			width:0px;
		}
		.tbl_lap1 .cel3{
			width:250px;
		}
		.baris td{
			padding:5px 10px;
		}
		
		.tbl_lap2{
			border-collapse:collapse;
			font-size:11px;
		}
		.tbl_lap2 .judul td{
			padding:3px 10px;
			text-align:center;
		}
		.tbl_lap2 .cel1{
			width:0px;
		}
		.tbl_lap2 .cel2{
			width:30px;
		}
		.tbl_lap2 .cel3{
			width:80px;
		}
		.tbl_lap2 .cel4{
			width:80px;
		}
		.tbl_lap2 .cel5{
			width:220px;
		}
		.tbl_lap3{
			margin:50px 0px;
		}
		.tbl_lap3 .left{
			width:242px;
			text-align:center;
			font-size:13px;
		}
		.tbl_lap3 .center{
			width:242px;
			text-align:center;
			font-size:13px;
		}
		.tbl_lap3 .right{
			width:242px;
			text-align:center;
			font-size:13px;
		}
		.catatan{
			font-size:10px;
			margin:10px 0px;
			line-height:20px;
		}
		.tbl_lap4{
			margin:10px 0px;
		}
		.tbl_lap4 td{
			padding:2px 0px;
		}
		.tbl_lap4 .cel1{
			width:200px;
		}
		.tbl_lap4 .cel2{
			width:200px;
		}
		.tbl_ver td{
			padding:5px;
		}
		</style>
	</head>
	<body>
		<div class="page">
		<div class="kops">
			<table class="kop">
				<tr>
					<td class="header">
						<img src="../../images/logo_perhunungan.png" class="logo"> 
					</td>
					<td  class="judul">
						<H3>SISTEM PERJALANAN DINAS PADA</H3>
						<h2>KANTOR DINAS MARTECH</h2>
                        
						<table>
							<tr>
								<td class="cel1">Jalan : <br>
								Medan, Indonesia
								</td>
								<td class="cel2">Telepon : 061-xxxx<br>
								061 - xxxx xxxx Ext xxxx<br>
								061 - xxxx xxxx Ext xxxx
								</td>
								<td class="cel3">Fax : 061-xxxxxxxx<br>
								Email : perjalanandinas@gmail.com
								</td>
							</tr>
						
						</table>
		
					</td>
				</tr>
			</table>
		</div>
		<div class="garis">
			<hr class="line1"/>
			<hr class="line2"/>
		</div>
		<div class="heading">
			<b><u>VERIFIKASI BIAYA PERJALANAN DINAS</u></b><br>
			<b><u>TAHUN ANGGARAN <?php echo date('Y') ?></u></b>
		</div>
		<?php
			$queryForm=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_provinsi, tbl_pegawai WHERE tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt='$id_spt' AND tbl_provinsi.id_provinsi=tbl_kegiatan.pergi");
			$rF=mysql_fetch_array($queryForm);
		?>
		<table class="tbl_lap1">
			<tr class="baris">
				<td class="cel1">Nama</td><td class="cel2">:</td><td class="cel3"><?php echo $rF['nama']; ?></td>
			</tr>
			<tr class="baris">
				<td>Golongan</td><td>:</td><td><?php echo $rF['pangkat']; ?> / <?php echo $rF['gol']; ?></td>
			</tr>
			<tr class="baris">
				<td>Unit Kerja</td><td>:</td><td><?php echo $rF['instansi']; ?></td>
			</tr>
			<tr class="baris">
				<td>Nomor SPT</td><td>:</td><td><?php echo $rF['no_kegiatan']; ?>, Tanggal <?php echo tgl_indo($rF['tgl_spt']); ?></td>
			</tr>
			<tr class="baris">
				<td>Nomor SPPD</td><td>:</td><td> <?php echo $rF['nomor_sppd']; ?></td>
			</tr>
			<tr class="baris">
				<td>Tujuan</td><td>:</td><td> <?php echo $rF['kota']; ?></td>
			</tr>
			<tr class="baris">
				<td>TMT SPPD</td><td>:</td><td> -</td>
			</tr>
			<tr class="baris">
				<td>Waktu Pelaksanaan</td><td>:</td><td> <?php echo tgl_indo($rF['tgl_berangkat']); ?> s/d <?php echo tgl_indo($rF['tgl_kembali']); ?></td>
			</tr>
			<tr class="baris">
				<td>Diterima Tim Verifikasi Tanggal</td><td>:</td><td> <?php echo tgl_indo($rF['tgl_modify']); ?></td>
			</tr>
			<tr class="baris">
				<td>Diserahkan Tanggal</td><td>:</td><td> <?php echo tgl_indo($rF['tgl_diserahkan']); ?></td>
			</tr>
		</table>
		<?php
			$queryForm=mysql_query("SELECT * FROM tbl_spt WHERE id_spt=$id_spt");
			$rF=mysql_fetch_array($queryForm);
		?>
		<table class="tbl_lap2" border="1">
			<tr class="judul" valign="middle">
				<td class="cel1">NO.</td><td class="cel2">URAIAN</td><td class="cel3">USULAN(Rp)</td><td class="cel4">DISETUJUI (Rp)</td><td class="cel5">KETERANGAN</td>
			</tr>
			<tr class="baris">
				<td>1</td><td>Uang Harian</td><td><?php echo format_rupiah($rF['usulan1']);  ?></td><td><?php echo format_rupiah($rF['setuju1']);  ?></td><td><?php echo $rF['kete1'];  ?></td>
			</tr>
			<tr class="baris">
				<td>2</td><td>Transportasi</td><td></td><td></td><td></td>
			</tr>
			<tr class="baris">
				<td></td><td>a. Dari Kantor ke Bandara/Terminal<br>/Stasiun/PP</td><td><?php echo format_rupiah($rF['usulan2']);  ?></td><td><?php echo format_rupiah($rF['setuju2']);  ?></td><td><?php echo $rF['kete2'];  ?></td>
			</tr>
			<tr class="baris">
				<td></td><td>b. Tiket</td><td><?php echo format_rupiah($rF['usulan3']);  ?></td><td><?php echo format_rupiah($rF['setuju3']);  ?></td><td><?php echo $rF['kete3'];  ?></td>
			</tr>
			<tr class="baris">
				<td></td><td>c. Lain-lain</td><td><?php echo format_rupiah($rF['usulan4']);  ?></td><td><?php echo format_rupiah($rF['setuju4']);  ?></td><td><?php echo $rF['kete4'];  ?></td>
			</tr>
			<tr class="baris">
				<td>3</td><td>Penginapan</td><td><?php echo format_rupiah($rF['usulan5']);  ?></td><td><?php echo format_rupiah($rF['setuju5']);  ?></td><td><?php echo $rF['kete5'];  ?></td>
			</tr>
			<tr class="baris">
				<td>4</td><td>Biaya Pengeluaran Riil</td><td><?php echo format_rupiah($rF['usulan6']);  ?></td><td><?php echo format_rupiah($rF['setuju6']);  ?></td><td><?php echo $rF['kete6'];  ?></td>
			</tr>
			<tr class="baris">
				<td>5</td><td>Uang Representatif</td><td><?php echo format_rupiah($rF['usulan7']);  ?></td><td><?php echo format_rupiah($rF['setuju7']);  ?></td><td><?php echo $rF['kete7'];  ?></td>
			</tr>
			
			<?php
			$queryla=mysql_query("SELECT * FROM tbl_spt WHERE id_spt='$id_spt'");
			$rla=mysql_fetch_array($queryla);
			
			if(empty($rla['la1'])){}else{
			?>
			<tr class="baris">
				<td>6</td><td><?php echo $rla['la1'] ?></td><td><?php echo format_rupiah($rla['la2']);  ?></td><td><?php echo format_rupiah($rla['la3']);  ?></td><td><?php echo $rla['la4'];  ?></td>
			</tr>
			<?php
			}
			?>
			
			<?php
			if(empty($rla['lb1'])){}else{
			?>
			<tr class="baris">
				<td>7</td><td><?php echo $rla['lb1'] ?></td><td><?php echo format_rupiah($rla['lb2']);  ?></td><td><?php echo format_rupiah($rla['lb3']);  ?></td><td><?php echo $rla['lb4'];  ?></td>
			</tr>
			<?php
			}
			?>
			
			<?php
			if(empty($rla['lc1'])){}else{
			?>
			<tr class="baris">
				<td>8</td><td><?php echo $rla['lc1'] ?></td><td><?php echo format_rupiah($rla['lc2']);  ?></td><td><?php echo format_rupiah($rla['lc3']);  ?></td><td><?php echo $rla['lc4'];  ?></td>
			</tr>
			<?php
			}
			?>
			
			<tr class="baris">
				<td colspan="2" align="center"><b>JUMLAH</b></td><td>
				<?php
				$hasil=$rF['usulan1']+$rF['usulan2']+$rF['usulan3']+$rF['usulan4']+$rF['usulan5']+$rF['usulan6']+$rF['usulan7']+$rla['la2']+$rla['lb2']+$rla['lc2'];
				echo format_rupiah($hasil);
				?>
				</td>
				<td>
				<?php
				$hasil1=$rF['setuju1']+$rF['setuju2']+$rF['setuju3']+$rF['setuju4']+$rF['setuju5']+$rF['setuju6']+$rF['setuju7']+$rla['la3']+$rla['lb3']+$rla['lc3'];
				echo format_rupiah($hasil1);
				
				$sisa=$rF['sudah_dibayar'];
				?>
				</td><td></td>
			</tr>
			
		</table>
		<table class="tbl_lap4">
			<tr>
				<td class="cel1">Ditetapkan Sejumlah</td><td class="cel2"><?php echo $n1=format_rupiah($hasil1); ?></td>
			</tr>
			<tr>
				<td>Yang telah dibayar semula</td><td><?php echo $n2=format_rupiah($sisa); ?> </td>
			</tr>
			<tr>
			<td></td><td><hr></td>
			</tr>
			<tr>
				<td>Sisa Kurang/lebih</td><td><?php echo format_rupiah($akhir=$sisa-$hasil1); ?></td>
			</tr>
		</table>
		<table class="tbl_lap3">
			<tr>
				<td class="left">
				<br>
					Mengetahui,<br>
					Pejabat Pembuat Komitmen<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
				
					
						<?php
							$ttd1=mysql_query("SELECT * FROM tbl_spt, tbl_pegawai WHERE tbl_spt.pem_komitmen=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
							$t=mysql_fetch_array($ttd1);
						?>
					
					<u><?php echo $t['nama']; ?></u><br>
					NIP. <?php echo $t['nip']; ?><br>
				</td>
				<td class="center" style="text-align:center;">	
					
					<br>
					<?php
							$ttd2=mysql_query("SELECT * FROM tbl_spt, tbl_pegawai WHERE tbl_spt.tim_verifikasi=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
							$t1=mysql_fetch_array($ttd2);
						?>
						<br>
					Tim Verifikasi<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
				
					
						
					
					<u><?php echo $t1['nama']; ?></u><br>
							NIP. <?php echo $t1['nip']; ?>
						sd
					
				</td>
				<?php
			$query=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_pegawai WHERE tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
			$rA=mysql_fetch_array($query);
		?>
				<td class="right">	
					<br>
					<?php echo tgl_indo($rF['tgl_diserahkan']); ?><br>
					Yang Bepergian<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					
					<u><?php echo $rA['nama']; ?></u><br>
					NIP. <?php echo $rA['nip']; ?><br>
				</td>
			</tr>
		</table>
		</div>
	</body>
</html>