<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	$idspt=$_GET['id_spt'];
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		*{
			color:#fff;
		}
		.cb{
			color:#000;
		}
		
		.tbl_lap1{
			margin:20px 0px;
		}
		.tbl_lap1 .cel1{
			width:180px;
		}
		.tbl_lap1 .cel2{
			width:200px;
		}
		.tbl_lap1 .cel3{
			width:290px;
		}
		.judul{
			margin:20px 0px;
			text-align:center;
		}
		.tbl_lap2{
			border-collapse:collapse;
			border:1px solid #000;
		}
		.tbl_lap2 .cel1{
			width:360px;
		}
		.tbl_lap2 .cel2{
			width:360px;
		}
		.tbl_lap2 td{
			padding:2px;
		}
		.space{
			padding:10px;
		}
		.tbl_isi .cel1{
			width:30px;
		}
		.tbl_isi .cel2{
			width:0px;
		}
		.tbl_isi .cel3{
			width:100px;
		}
		.tbl_isi td{
			font-size:10px;
		}
		.tbl_lap3 .cel1{
			width:300px;
		}
		.tbl_lap3 .cel2{
			width:180px;
		}
		.tbl_lap3 .cel3{
			width:300px;
		}
		</style>
	</head>
	<body>
		<div class="page">
			
			
			<table class="tbl_lap2" border="0px" style="margin-top:10px;">
				
			<?php
				$query=mysql_query("SELECT * FROM tbl_spt WHERE id_spt='$idspt'");
				$r=mysql_fetch_array($query);
			?>
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">I.</td><td class="cel2">Tiba di</td><td class="cel3">: <span class='cb'><?php echo $r['men1']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>: <span class='cb'><?php echo tgl_indo($r['men2']); ?></span></td>
							</tr>
							
							<tr>
								<td></td><td colspan="2" style="text-align:left;"><span class='cb'><?php echo $r['men3']; ?></span><br><br><br><br><br><br><br><span class='cb'><?php echo $r['men4']; ?></span><br><span class='cb'>NIP. <?php echo $r['men5']; ?></span></td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2">Berangkat dari</td><td class="cel3">: <span class='cb'><?php echo $r['men6']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>: <span class='cb'><?php echo $r['men7']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>: <span class='cb'><?php echo tgl_indo($r['men8']); ?></span></td>
							</tr>
							<tr>
								<td></td><td colspan="2"><span class='cb'><?php echo $r['men9']; ?></span><br><br><br><br><br><br><span class='cb'><?php echo $r['men10']; ?></span><br><span class='cb'>NIP. <?php echo $r['men11']; ?></span></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">II.</td><td class="cel2">Tiba di</td><td class="cel3">: <span class='cb'><?php echo $r['men12']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>: <span class='cb'><?php echo tgl_indo($r['men13']); ?></span></td>
							</tr>
							
							<tr>
								<td></td><td colspan="2"><span class='cb'><?php echo $r['men14']; ?></span><br><br><br><br><br><br><br><br><span class='cb'><?php echo $r['men15']; ?></span><br><span class='cb'>NIP. <?php echo $r['men16']; ?></span></td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2">Berangkat dari</td><td class="cel3">: <span class='cb'><?php echo $r['men17']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>: <span class='cb'><?php echo $r['men18']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>: <span class='cb'><?php echo tgl_indo($r['men19']); ?></span></td>
							</tr>
							<tr>
								<td></td><td colspan="2"><span class='cb'><?php echo $r['men20']; ?></span><br><br><br><br><br><br><br><span class='cb'><?php echo $r['men21']; ?></span><br><span class='cb'>NIP. <?php echo $r['men22']; ?></span></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">I.</td><td class="cel2">Tiba di</td><td class="cel3">: <span class='cb'><?php echo $r['men23']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>: <span class='cb'><?php echo tgl_indo($r['men24']); ?></span></td>
							</tr>
							
							<tr>
								<td></td><td colspan="2"><span class='cb'><?php echo $r['men25']; ?></span><br><br><br><br><br><br><br><br><span class='cb'><?php echo $r['men26']; ?></span><br><span class='cb'>NIP. <?php echo $r['men27']; ?></span></td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2">Berangkat dari</td><td class="cel3">: <span class='cb'><?php echo $r['men28']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>: <span class='cb'><?php echo $r['men29']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>: <span class='cb'><?php echo tgl_indo($r['men30']); ?></span></td>
							</tr>
							<tr>
								<td></td><td colspan="2"><span class='cb'><?php echo $r['men31']; ?></span><br><br><br><br><br><br><br><span class='cb'><?php echo $r['men32']; ?></span><br><span class='cb'>NIP. <?php echo $r['men33']; ?></span></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">I.</td><td class="cel2">Tiba di</td><td class="cel3">: <span class='cb'><?php echo $r['men34']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>: <span class='cb'><?php echo tgl_indo($r['men35']); ?></span></td>
							</tr>
							
							<tr>
								<td></td><td colspan="2"><span class='cb'><?php echo $r['men36']; ?></span><br><br><br><br><br><br><br><br><span class='cb'><?php echo $r['men37']; ?></span><br><span class='cb'><?php echo $r['men38']; ?></span></td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2">Berangkat dari</td><td class="cel3">: <span class='cb'><?php echo $r['men39']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>: <span class='cb'><?php echo $r['men40']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>: <span class='cb'><?php echo tgl_indo($r['men41']); ?></span></td>
							</tr>
							<tr>
								<td></td><td colspan="2"><span class='cb'><?php echo $r['men42']; ?></span><br><br><br><br><br><br><br><span class='cb'><?php echo $r['men43']; ?></span><br><span class='cb'><?php echo $r['men44']; ?></span></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">V.</td><td class="cel2">Tiba di</td><td class="cel3">: <span class='cb'><?php echo $r['men45']; ?></span></td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>: <span class='cb'><?php echo $r['men46']; ?></span></td>
							</tr>
							
							<tr>
								<td></td><td colspan="2"><span class='cb'><?php echo $r['men47']; ?></span><br><br><br><br><br><br><br><span class='cb'><?php echo $r['men48']; ?></span><br><span class='cb'><?php echo $r['men49']; ?></span></td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2" colspan="2">
								Telah diperiksa dengan keterangan bahwa perjalanan<br>
								tersebut atas perintahnya semata-mata untuk<br>
								kepentingan jabatan dalam waktu yang sesingkat-<br>
								singkatnya.</td>
							</tr>
							<tr>
								<td></td><td colspan="2">Pejabat Pembuat Komitmen<br><br><br><br><br><br><span class='cb'><?php echo $r['men50']; ?></span><br><span class='cb'><?php echo $r['men51']; ?></span></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1" colspan="2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">VI.</td><td class="cel2" colspan="2"><span class='cb'>Catatan lain-lain</span></td><td class="cel3">: </td><td><span class='cb'><?php echo $r['men52']; ?></span></td>
							</tr>
							
						</table>
					</td>
				
				</tr>
				<tr>
					<td class="cel1" colspan="2">
						<br>
                        <br>
                        <br>
                     
					</td>
				
				</tr>
				
				
			</table>
			
			
		</div>
	</body>
</html>