<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");

	$no_spt=$_GET['no_spt'];
	$sql="SELECT * FROM spt a LEFT JOIN ts b ON a.no_ts=b.no_ts
				LEFT JOIN instansi c ON b.kd_instansi=c.kd_instansi
				WHERE no_spt='$no_spt'";
	$q=mysql_query($sql) or die(mysql_error());
	$f=mysql_fetch_array($q);
	extract($f);
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		html{
			font-family: sans-serif;
		}
		body{
			padding: 10px 30px !important;
		}
    .tbl_field td{
        border-bottom: 2px solid #999;
    }
		.logo{
				width:80px;
				height:80px;
				margin-top: 10px
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;

		}
		.kop .judul{
			text-align:center;

		}
		.judul p{
			margin: 0;
			padding: 0
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		 table td{
			font-size:13px !important;
		}

		.garis{
			line-height:1.5px;
		}
		.garis .line1{
			border:1px solid #000;
		}
		.garis .line2{
			border:0.5px solid #000;
		}

		.baris td{
			padding:10px 8px;
		}
    .nomor{
      text-align: center;
    }
		.judul h3{
			padding: 0;
      margin: 0;
			font-size: 22px
  }
    .nomor p{
      padding: 0;
      margin: 0
    }
    .nomor h3{
      padding: 0;
      margin: 0
    }
    table{
      width: 100%;
      border-spacing: 0;
    }
    table tr td{
      vertical-align: top;
			padding-bottom: 10px
    }
    .width100{
      width: 100px;
    }
    .width150{
      width: 150px
    }
    .width10{
      width: 10px
    }
    .pading10 tr td{
      padding: 10px
    }
    .paddingbottom10 tr td{
      padding-bottom: 10px
    }
    .paddingbottom20 tr td{
      padding-bottom: 20px
    }
    .tablesub tr td{
      padding-bottom: 5px
    }
		.ttd tr td{
			width: 30%
		}
		.center{
			text-align: center;
		}
		.border td{
			border: 1px solid #000
		}
		.table tr td{
			width: 50%;
			padding: 4px
		}
		.table tr td table{
			width: 200px
		}
		.table tr td table tr td:nth-child(1){
			width: 1px
		}
		.table tr td table tr td:nth-child(2){
			width: 130px;
			/*border: 1px solid #000*/
		}
		.table tr td table tr td:nth-child(3){
			width: 10px;
			text-align: left
		}
		</style>
	</head>
	<body>
		<div class="page">


			<table class="table" border="1px" style="margin-top:30px;">
				<tr>
					<td>
						<table border="0">
							<tr>
								<td>I.</td><td>Tiba di</td><td>:</td>
								<td><?=$tempat_tujuan?></td>

							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
								<td style="width:100px !important"><?=tgl_indo($tgl_mulai)?></td>

							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>

						<div class="center">

							Mengetahui,<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</div>
					</td>
					<td>
						<table border="0">
							<tr>
								<td></td>
								<td>Berangkat dari</td>
								<td>:</td>
								<td><?=$tempat_tujuan?></td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>:</td>
								<td><?=$tempat?></td>

							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
								<td style="width:100px !important"><?=tgl_indo($tgl_selesai)?></td>

							</tr>
						</table>
						<div class="center">
							Mengetahui,<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0">
							<tr>
								<td>II.</td><td>Tiba di</td><td>:</td>
							</tr>
							<tr>
								<td></td>
								<td>Pada Tanggal</td>
								<td>:</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>

						</table>

						<div class="center">
							Mengetahui,<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</div>
					</td>
					<td>
						<table border="0">
							<tr>
								<td></td><td>Berangkat dari</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
						</table>

						<div class="center">
							Mengetahui,<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0">
							<tr>
								<td>III.</td><td>Tiba di</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>

						<div class="center">
							Mengetahui,<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</div>
					</td>
					<td>
						<table border="0">
							<tr>
								<td></td><td>Berangkat dari</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
						</table>

						<div class="center">
							Mengetahui,<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0">
							<tr>
								<td>IV.</td><td>Tiba di</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>

						<div class="center">
							Mengetahui,<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</div>
					</td>
					<td>
						<table border="0">
							<tr>
								<td></td><td>Berangkat dari</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
						</table>

						<div class="center">
							Mengetahui,<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0">
							<tr>
								<td>V.</td><td>Tiba di</td><td>:</td>
								<td><?=$tempat?></td>

							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
								<td style="width:100px"><?=tgl_indo($tgl_selesai)?></td>
							</tr>

							<tr>
								<td></td><td colspan="2"><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
					<td>
						<table border="0" style="width:100% !important">
							<tr>
								<td colspan="2" style="width:200px !important">
								Telah diperiksa dengan keterangan bahwa perjalanan<br>
								tersebut atas perintahnya semata-mata untuk<br>
								kepentingan jabatan dalam waktu yang sesingkat-<br>
								singkatnya.</td>
							</tr>
							<tr>
								<td colspan="2" class="center" style="padding:0 50px">
									<br>
									Pejabat Pembuat Komitmen<br>
									<?=strtoupper('Kepala '.$f['nm_instansi'])?>
									<br>
									<br>
									<br>
									<br>
									<br>
									<u><?php echo $f['kepala_dinas']; ?></u>
									<br>
									NIP. <?php echo $f['NIP']; ?>
								 </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						VI.
					</td>

				</tr>
				<tr>
					<td colspan="2">
						VIII. PERTHATIAN :<br>
								PPK yang menerbitkan SPD, pegawai yang melakukan perjalanan dinas, para pejabat yang mengesahkan<br>
								tanggal berangkat/tiba, serta bendahara pengeluaran pertanggung jawab berdasarkan peraturan-<br>
								peraturan Keuangan Negara apabila negara menderita rugi akibat kesalahan, kelalaian, dan<br>
								kealpaanya.
					</td>

				</tr>


			</table>


		</div>
	</body>
</html>
