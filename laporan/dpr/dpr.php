<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	include ("../../config/fungsi_rupiah.php");
	$id_spt=$_GET['id_spt'];
	$tgl_sek=$_POST['tgl_sekarang'];
	
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		.logo{
				width:100px;
				height:100px;
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;
		
		}
		.kop .judul{
			text-align:center;
			
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}
		
	
			
			.garis{
				line-height:1.5px;
			}
			.garis .line1{
				border:1px solid #000;
			}
			.garis .line2{
				border:0.5px solid #000;
			}
		.tbl_lap1{
			margin:20px 0px;
		}
		.tbl_lap1 .cel1{
			width:180px;
		}
		.tbl_lap1 .cel2{
			width:200px;
		}
		.tbl_lap1 .cel3{
			width:290px;
		}
		.judul{
			margin:20px 0px;
			text-align:center;
			font-size:16px;
		}
		.tbl_lap2{
			border-collapse:collapse;
			border:1px solid #000;
		}
		.tbl_lap2 .cel1{
			width:20px;
		}
		.tbl_lap2 .cel2{
			width:200px;
		}
		.tbl_lap2 .cel3{
			width:450px;
		}
		.tbl_lap2 td{
			padding:5px;
		}
		.space{
			padding:10px;
		}
		.tbl_i{
			margin-top:15px;
			border-collapse:collapse;
		}
		.tbl_i .cel1{
			width:20px;
		}
		.tbl_i .cel2{
			width:400px;
		}
		.tbl_i .cel3{
			width:200px;
		}
		.tbl_i .issi{
			height:100px;
		}
		
		.tbl_lap3 .cel1{
			width:250px;
		}
		.tbl_lap3 .cel2{
			width:200px;
		}
		.tbl_lap3 .cel3{
			width:250px;
		}
		</style>
	</head>
	<body>
		<div class="page">
			<div class="kops">
			<table class="kop">
				<tr>
					<td class="header">
						<img src="../../images/logo_perhunungan.png" class="logo"> 
					</td>
					<td  class="judul">
						<H3>SISTEM PERJALANAN DINAS PADA</H3>
						<h2>KANTOR DINAS MARTECH</h2>
                        
						<table>
							<tr>
								<td class="cel1">Jalan : <br>
								Medan, Indonesia
								</td>
								<td class="cel2">Telepon : 061-xxxx<br>
								061 - xxxx xxxx Ext xxxx<br>
								061 - xxxx xxxx Ext xxxx
								</td>
								<td class="cel3">Fax : 061-xxxxxxxx<br>
								Email : perjalanandinas@gmail.com
								</td>
							</tr>
						
						</table>
		
					</td>
				</tr>
			</table>
		</div>
		<div class="garis">
			<hr class="line1"/>
			<hr class="line2"/>
		</div>
			<?php
			$queryForm=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_pegawai WHERE tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
			$rF=mysql_fetch_array($queryForm);
		?>
			<div class="judul"><u>DAFTAR PENGELUARAN RIIL</u></div>
			<table class="tbl_lap2">
				<tr>
					<td class="cc" colspan="3">Yang bertandatangan di bawah ini :</td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Nama</td><td class="cel3"> : <?php echo $rF['nama']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">NIP</td><td class="cel3"> : <?php echo $rF['nip']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Jabatan</td><td class="cel3"> : <?php echo $rF['jabatan']; ?></td>
				</tr>
				<tr>
					<td class="cc" colspan="3" align='justify'>berdasarkan Surat Perjalanan Dinas (SPD) Nomor <b><?php echo $rF['no_kegiatan']; ?></b> tanggal <b><?php echo tgl_indo($rF['tgl_spt']); ?></b>, dengan ini kami menyatakan dengan sesungguhnya bahwa :</td>
				</tr>
				<tr>
					<td class="cel1">1</td><td colspan="2">Biaya transport pegawai dan / atau biaya penginapan di bawah ini yang tidak dapat diperoleh bukti-bukti pengeluaranya, meliputi :<br>
					<table class="tbl_i" border="1px">
						<tr>
							<td class="cel1" align="center">No.</td><td class="cel2" align="center">Uraian</td><td class="cel3" align="center">Jumlah</td>
						</tr>
						<tr>
				<td>1</td><td>Uang Harian</td><td><?php echo format_rupiah($rF['setuju1']);  ?></td>
			</tr>
			
			<tr>
			<td>2</td>
				<td>
				a. Dari Kantor ke Bandara/Terminal/Stasiun/PP<br>
				b. Tiket<br>
				c. Lain-lain<br>
				</td><td><?php echo format_rupiah($rF['setuju2']);  ?> <br> <?php echo format_rupiah($rF['setuju3']);  ?> <br> <?php echo format_rupiah($rF['setuju4']);  ?></td>
			</tr>
			
			<tr>
				<td>3</td><td>Penginapan</td><td><?php echo format_rupiah($rF['setuju5']);  ?></td>
			</tr>
			<tr>
				<td>4</td><td>Biaya Pengeluaran Riil</td><td><?php echo format_rupiah($rF['setuju6']);  ?></td>
			</tr>
			<tr>
				<td>5</td><td>Uang Representatif</td><td><?php echo format_rupiah($rF['setuju7']);  ?></td>
			</tr>
			<?php
			if(empty($rF['la3'])){}else{
			?>
			<tr>
				<td>6</td><td><?php echo $rF['la1'];  ?></td><td><?php echo format_rupiah($rF['la3']);  ?></td>
			</tr>
			<?php
			}
			?>
			<?php
			if(empty($rF['lb3'])){}else{
			?>
			<tr>
				<td>7</td><td><?php echo $rF['lb1'];  ?></td><td><?php echo format_rupiah($rF['lb3']);  ?></td>
			</tr>
			<?php
			}
			?>
			<?php
			if(empty($rF['lc3'])){}else{
			?>
			<tr>
				<td>8</td><td><?php echo $rF['lc1'];  ?></td><td><?php echo format_rupiah($rF['lc3']);  ?></td>
			</tr>
			<?php
			}
			?>
						
						<tr>
							<td></td><td align="right">JUMLAH</td><td><?php
				$hasil1=$rF['setuju1']+$rF['setuju2']+$rF['setuju3']+$rF['setuju4']+$rF['setuju5']+$rF['setuju6']+$rF['setuju7']+$rF['la3']+$rF['lb3']+$rF['lc3'];
				echo format_rupiah($hasil1);
				
				
				?></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="cel1">2</td><td colspan="2" style="width:200px;text-algin:justify;">Jumlah uang tersebut pada angka 1 di atas benar-benar dikeluarkan untuk pelaksanaan Perjalanan Dinas dimaksud dan apabila di kemudian hari terdapat kelebihan atas pembayaran, kami bersedia menyetorkan kelebihan tersebut ke Kas Negara.
					<br>Demikian pernyataan ini kami buat dengan sebenarnya, untuk dipergunakan sebagaimana mestinya.
					</td>
				</tr>
				
			</table>

			<table class="tbl_lap3" border="0px">
				
				<tr>
					<td class="cel1">
						Mengetahui / Menyetujui,<br>
						Pejabat Pembuat Komitmen,<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						
						<br>
						<?php
							$ttd1=mysql_query("SELECT * FROM tbl_spt, tbl_pegawai WHERE tbl_spt.pem_komitmen=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
							$t=mysql_fetch_array($ttd1);
						?>
						<?php echo $t['nama']; ?><br>
						NIP <?php echo $t['nip']; ?><br>

					</td>
					<td class="cel2"></td>
					<td class="cel3">
					<?php
			$query=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_pegawai WHERE tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
			$rA=mysql_fetch_array($query);
		?>
						Medan, <?php echo tgl_indo($tgl_sek); ?><br>
						Pelaksanaan SPD,<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						
						<br>
						<?php echo $rA['nama']; ?><br>
						NIP. <?php echo $rA['nip']; ?><br>
					</td>
				</tr>
			</table>
			
			
		</div>
	</body>
</html>