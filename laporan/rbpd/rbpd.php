<?php
		include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	include ("../../config/fungsi_rupiah.php");
	$id_spt=$_GET['id_spt'];
	$tgl_sek=$_POST['tgl_sekarang'];
	$bendahara=$_POST['bendahara'];
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		.logo{
				width:100px;
				height:100px;
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;
		
		}
		.kop .judul{
			text-align:center;
			
			
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}
		
	
			
			.garis{
				line-height:1.5px;
			}
			.garis .line1{
				border:1px solid #000;
			}
			.garis .line2{
				border:0.5px solid #000;
			}
		.tbl_lap1{
			margin:20px 0px;
		}
		.tbl_lap1 .cel1{
			width:180px;
		}
		.tbl_lap1 .cel2{
			width:200px;
		}
		.tbl_lap1 .cel3{
			width:290px;
		}
		.judul{
			margin:20px 0px;
			text-align:center;
		}
		.tbl_lap2{
			border-collapse:collapse;
			border:1px solid #000;
			margin:0px 15px 15px 15px;
		}
		.tbl_lap2 .cel1{
			width:20px;
		}
		.tbl_lap2 .cel2{
			width:150px;
		}
		.tbl_lap2 .cel3{
			width:100px;
		}
		.tbl_lap2 .cel4{
			width:400px;
		}
		.tbl_lap2 td{
			padding:4px 2px;
			font-size:11px;
		}
		.space{
			padding:10px;
		}
            .tbl_lap3{
                margin:0px 15px 15px 15px;
            }
		.tbl_lap3 .cel1{
			width:300px;
		}
		.tbl_lap3 .cel2{
			width:100px;
		}
		.tbl_lap3 .cel3{
			width:300px;
		}
		.lamp .cel1{
			width:180px;
		}
		.lamp .cel2{
			width:180px;
		}
		.garis{
            margin:20px 15px 15px 15px;
			line-height:1px;
		
		}
		.tbl_lap4 .cel1{
			width:60px;
		}
		.tbl_lap4 .cel2{
			width:240px;
		}
		.tbl_lap4 .cel3{
			width:30px;
		}
		.tbl_lap4 td{
			padding:5px 3px;
		}
		.ttd{
			margin-top:100px;
            
		}
		.ttd .cel1{
			width:300px;
		}
		.ttd .cel2{
			width:100px;
		}
		.ttd .cel3{
			width:300px;
		}
            .page{
                border: 1px solid #000;
                padding: 0px;
            }
		</style>
	</head>
	<body>
		<div class="page">
			
			
			<?php
			$queryForm=mysql_query("SELECT * FROM tbl_spt WHERE id_spt=$id_spt");
			$rF=mysql_fetch_array($queryForm);
		?>
			<div class="judul"><b><u>RINCIAN BIAYA PERJALANAN DINAS</u></b></div>
			<table class="lamp">
				<tr>
					<td class="cel1"></td><td class="cel2">Lampiran SPPD Nomor</td><td class="cel3"> : <?php echo $rF['nomor_sppd'] ?></td>
				</tr>
				<tr>
					<td></td><td>Tanggal</td><td> : <?php echo tgl_indo($tgl_sek) ?></td>
				</tr>
			</table>
			
			<table class="tbl_lap2" border="1">
				<tr>
					<td class="cel1">NO.</td><td class="cel2">RINCIAN BIAYA</td><td class="cel3" align="center">JUMLAH</td><td class="cel4" align="center">KETERANGAN</td>
				</tr>
			</table>
			<table class="tbl_lap2" border="1">
				<tr>
					<td class="cel1">1.</td><td class="cel2">Uang Harian</td><td class="cel3"><?php echo format_rupiah($rF['setuju1']);  ?></td><td class="cel4"><?php echo $rF['kete1'] ?></td>
				</tr>
				<tr>
					<td class="cel1">2.</td><td class="cel2"> Dari Kantor ke Bandara / <br>Terminal
/ tasiun / PP
</td><td class="cel3"><?php echo format_rupiah($rF['setuju2']);  ?></td><td class="cel4"><?php echo $rF['kete2'] ?></td>
				</tr>
				<tr>
					<td class="cel1">3.</td><td class="cel2">Tiket</td><td class="cel3"><?php echo format_rupiah($rF['setuju3']);  ?></td><td class="cel4"><?php echo $rF['kete3'] ?></td>
				</tr>
				<tr>
					<td class="cel1">4.</td><td class="cel2">Biaya Pengeluaran Riil </td><td class="cel3"><?php echo format_rupiah($rF['setuju4']);  ?></td><td class="cel4"><?php echo $rF['kete4'] ?></td>
				</tr>
				<tr>
					<td class="cel1">5.</td><td class="cel2">Penginapan</td><td class="cel3"><?php echo format_rupiah($rF['setuju5']);  ?></td><td class="cel4"><?php echo $rF['kete5'] ?></td>
				</tr>
				<tr>
					<td class="cel1">6.</td><td class="cel2">Transportasi/Taxi/Bus</td><td class="cel3"><?php echo format_rupiah($rF['setuju6']);  ?></td><td class="cel4"><?php echo $rF['kete6'] ?></td>
				</tr>
				<tr>
					<td class="cel1">7.</td><td class="cel2">Uang Representatif</td><td class="cel3"><?php echo format_rupiah($rF['setuju7']);  ?></td><td class="cel4"><?php echo $rF['kete7'] ?></td>
				</tr>
				
				<tr>
					<td class="cel1">8.</td><td class="cel2"><?php echo $rF['la1'] ?></td><td class="cel3"><?php echo format_rupiah($rF['la3']);  ?></td><td class="cel4"><?php echo $rF['la4'] ?></td>
				</tr>
				<tr>
					<td class="cel1">9.</td><td class="cel2"><?php echo $rF['lb1'] ?></td><td class="cel3"><?php echo format_rupiah($rF['lb3']);  ?></td><td class="cel4"><?php echo $rF['lb4'] ?></td>
				</tr>
				<tr>
					<td class="cel1">10.</td><td class="cel2"><?php echo $rF['lc1'] ?></td><td class="cel3"><?php echo format_rupiah($rF['lc3']);  ?></td><td class="cel4"><?php echo $rF['lc4'] ?></td>
				</tr>
				
				
				<tr>
					<td class="cel1" colspan="2">JUMLAH</td><td class="cel3">
					<?php
				$hasil1=$rF['setuju1']+$rF['setuju2']+$rF['setuju3']+$rF['setuju4']+$rF['setuju5']+$rF['setuju6']+$rF['setuju7']+$rF['la3']+$rF['lb3']+$rF['lc3'];
				echo format_rupiah($hasil1);
				?>
					</td><td class="cel4"></td>
				</tr>
			</table>
			<p class="space"></p>
			<table class="tbl_lap3" border="0px">	
				<tr>
					<td class="cel1">
					<br>
					<br>
					Biaya Perjalanan : <br>
					
					<b><?php echo format_rupiah($hasil1); ?></b><br>
					<br>
					<br>
					<br>
					Bendahara Penggeluaran,
					<br>
					<br>
					<br>
					<br>
					<br>
					<?php
					$quben=mysql_query("SELECT * FROM tbl_pegawai WHERE id_pegawai='$bendahara'");
					$rben=mysql_fetch_array($quben);
					?>
					<?php echo $rben['nama']; ?><br>
							NIP. <?php echo $rben['nip']; ?>
					</td>
					
					<td class="cel2"></td><td class="cel3">
					Medan, <?php echo tgl_indo($tgl_sek); ?><br><br>
					Telah menerima Jumlah Uang Sebesar : <br>
					
					<b><?php echo format_rupiah($rF['sudah_dibayar']); ?></b><br>
					<br>
					<br>
					<br>
					Yang Menerima,
					<br>
					<br>
					<br>
					<br>
					<br>
						<?php
			$query=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_pegawai WHERE tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
			$rA=mysql_fetch_array($query);
			echo $rA['nama']; ?>
						<br>NIP. <?php echo $rA['nip']; ?>
					</td>
				</tr>
			</table>
			<div class="garis">
				<hr>
				<hr>
			</div>
			<div class="judul">PERHITUNGAN SPPD RAMPUNG</div>
			<table class="tbl_lap4">
				<tr>
					<td class="cel1"></td><td class="cel2">Biaya yang ditetapkan sejumlah</td><td class="cel3">:</td><td class="cel4"><?php echo $a=format_rupiah($hasil1); ?></td>
				</tr>
				
				<tr>
					<td></td><td>Biaya yang telah dibayarkan semula</td><td>:</td><td><?php echo $b=format_rupiah($rF['sudah_dibayar']); ?></td>
				</tr>
				<tr>
                    <?php
                        $hasil=$rF['sudah_dibayar']-$hasil1;
                    ?>
					<td></td><td>Sisa kurang / lebih</td><td>:</td><td><?php echo format_rupiah($hasil); ?> </td>
				</tr>
			</table>
			
			<table class="ttd" border="0px">
				<tr>
					<td class="cel1"></td><td class="cel2"></td><td class="cel3" align="center">PEJABAT PEMBUAT KOMITMEN
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<?php
							$ttd1=mysql_query("SELECT * FROM tbl_spt, tbl_pegawai WHERE tbl_spt.pem_komitmen=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
							$t=mysql_fetch_array($ttd1);
						?>
					
					<u><?php echo $t['nama']; ?></u><br>
					NIP. <?php echo $t['nip']; ?><br>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>