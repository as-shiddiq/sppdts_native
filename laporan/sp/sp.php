<?php
	include ("../../config/koneksi.php");
	$id_spt=$_GET['id_spt'];
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
				.logo{
				width:100px;
				height:100px;
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;
		
		}
		.kop .judul{
			text-align:center;
			
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}
		
		.garis{
			line-height:1.5px;
		}
		.garis .line1{
			border:1px solid #000;
		}
		.garis .line2{
			border:0.5px solid #000;
		}
		.heading{
			
			text-align:center;
			font-size:13px;
		}
		.b{
			font-style:bold;
		}
		.tbl_lap1{
			margin-bottom:50px;
			margin-top:50px;
		}
		.tbl_lap1 .cel1{
			width:170px;
		}
		.tbl_lap1 .cel2{
			width:0px;
		}
		.tbl_lap1 .cel3{
			width:450px;
		}
		.baris td{
			padding:5px 10px;
		}
		
		.tbl_lap2{
			border-collapse:collapse;
		}
		.tbl_lap2 .judul td{
			padding:3px 10px;
			text-align:center;
		}
		.tbl_lap2 .cel1{
			width:0px;
		}
		.tbl_lap2 .cel2{
			width:280px;
		}
		.tbl_lap2 .cel3{
			width:80px;
		}
		.tbl_lap2 .cel4{
			width:100px;
		}
		.tbl_lap2 .cel5{
			width:80px;
		}
		.tbl_lap3{
			margin:50px 0px;
		}
		.tbl_lap3 .left{
			width:220px;
			text-align:center;
			font-size:13px;
		}
		.tbl_lap3 .center{
			width:220px;
			text-align:center;
			font-size:13px;
		}
		.tbl_lap3 .right{
			width:220px;
			text-align:center;
			font-size:13px;
		}
		.catatan{
			font-size:10px;
			margin:10px 0px;
			line-height:20px;
		}
		.desk{
			margin-top:50px;
			padding:10px;
			text-align:justify;
			line-height:25px;
		}
		</style>
	</head>
	<body>
		<div class="page">
		<div class="kops">
			<table class="kop">
				<tr>
					<td class="header">
						<img src="../../images/logo_perhunungan.png" class="logo"> 
					</td>
					<td  class="judul">
						<H3>SISTEM PERJALANAN DINAS PADA</H3>
						<h2>KANTOR DINAS MARTECH</h2>
                        
						<table>
							<tr>
								<td class="cel1">Jalan : <br>
								Medan, Indonesia
								</td>
								<td class="cel2">Telepon : 061-xxxx<br>
								061 - xxxx xxxx Ext xxxx<br>
								061 - xxxx xxxx Ext xxxx
								</td>
								<td class="cel3">Fax : 061-xxxxxxxx<br>
								Email : perjalanandinas@gmail.com
								</td>
							</tr>
						
						</table>
		
					</td>
				</tr>
			</table>
		</div>
		<div class="garis">
			<hr class="line1"/>
			<hr class="line2"/>
		</div>
		<div class="heading">
			<b><h3><u>SURAT PERNYATAAN</u></h3></b>
			
		</div>
		<?php
			$queryForm=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_pegawai, tbl_provinsi WHERE tbl_provinsi.id_provinsi=tbl_kegiatan.pergi AND tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
			$rF=mysql_fetch_array($queryForm);
		?>
		<table class="tbl_lap1">
			<tr class="baris">
				<td class="cel1" colspan="3"><b>Yang bertanda tangan di bawah ini:</b></td>
			</tr>
			<tr class="baris">
				<td>Nama</td><td>:</td><td class="cel3"><?php echo $rF['nama']; ?></td>
			</tr>
			<tr class="baris">
				<td>NIP</td><td>:</td><td class="cel3"><?php echo $rF['nip']; ?></td>
			</tr>
			<tr class="baris">
				<td>Pangkat / Golongan</td><td>:</td><td class="cel3"><?php echo $rF['pangkat']; ?> / <?php echo $rF['gol']; ?></td>
			</tr>
			<tr class="baris">
				<td>Jabatan</td><td>:</td><td class="cel3"><?php echo $rF['jabatan']; ?></td>
			</tr>
			<tr class="baris">
				<td>Nomor SPT</td><td>:</td><td class="cel3"> <?php echo $rF['no_kegiatan']; ?></td>
			</tr>
			<tr class="baris">
				<td>Lokasi Penugasan</td><td>:</td><td class="cel3"> <?php echo $rF['nama_provinsi']; ?></td>
			</tr>
			
		</table>
		<div class="desk">
			Dengan ini menyatakan bahwa dokumen/berkas-berkas pertanggung jawaban perjalanan dinas yang saya
			sampaikan sebagaimana terlampir adalah benar dan dapat dipertanggung jawabkan Apabila bukti-bukti
			tersebut tidak benar, maka saya bersedia dekenakan sanksi sesuai dengan ketentuan perundang-undagan
			yang berlaku.
		</div>		
		<table class="tbl_lap3">
			<tr>
				<td class="left">
				
				</td>
				<td class="center">
				
				</td>
				<td class="right">	
					<br>
					<br>
					Yang Membuat Pernyataan,<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<u><?php echo $rF['nama']; ?></u><br>
					NIP. <?php echo $rF['nip']; ?><br>
				</td>
			</tr>
		</table>
		</div>
	</body>
</html>