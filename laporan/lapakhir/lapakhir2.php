<?php
error_reporting(0);
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	include ("../../config/fungsi_rupiah.php");
	$mulai=$_GET['mulai'];
	$log=$_GET['onlog'];
	$bagian=$_GET['bagian'];
	
	$tgl1=$_GET['tgl1'];
	$tgl2=$_GET['tgl2'];
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		.judul{
			margin:20px 0px;
			text-align:center;
			font-size:13px;
		}
		
		.kete .cel1{
			width:10px;
		}
		.kete .cel2{
			width:900px;
		}
		.kete td{
			padding:5px;
		}
		.tbl_ttd td{
			font-size:11px;
		}
		.tbl_ttd .cel1{
			width:500px;
		}
		.tbl_ttd .cel2{
			width:300px;
		}
		.tbl_ttd .cel3{
			width:100px;
		}
		.tbl_rekap{
			border-collapse:collapse;
			border:1px solid #000;
		}
		.tbl_rekap .head td{
			padding:5px;
			font-size:7px;
		}
		.tbl_rekap .body td{
			
			font-size:7px;
		}
		
		.cls1{
			width:0px;
			padding:5px;
		}
		.cls2{
			width:100px;
			padding:5px;
		}
		.cls3{
			width:40px;
			text-align:center;
			padding:5px;
		}
		.cls4{
			width:90px;
			padding:5px;
		}
		.cls5{
			width:10px;
			
		}
		.cls6{
			width:150px;
		}
		.cls7{
			width:80px;
		}
		.cls8{
			width:40px;
		}
		.cls9{
			width:31px;
		}
		.cls10{
			width:100px;
		}
		.cls11{
			width:50px;
		}
		.child{
			border-collapse:collapse;
			border:1px solid #000;
		}
		.child td{
			padding:10px;
		}
		.cari{
			font-size:11px;
		}
		
		</style>
	</head>
	<body>
	<?php 
			$contgl=substr($tgl1,3,2);
			$conbln=substr($tgl1,0,2);
			$conthn=substr($tgl1,6,9);
			$tglcon1=$contgl."/".$conbln."/".$conthn;
			
			$contgl2=substr($tgl2,3,2);
			$conbln2=substr($tgl2,0,2);
			$conthn2=substr($tgl2,6,9);
			$tglcon2=$contgl2."/".$conbln2."/".$conthn2;
			?>
        
		<div class="pot">
			<div class="judul">REKAP PERJALANAN DINAS<br> KANTOR DINAS MARTECH <br> TAHUN ANGGARAN <?php echo $mulai ?><br>
			</div>
            <?php
                if($tgl1=='' AND $tgl2==''){
                    
                }
                else{
            ?>
			<div class='cari'>
			Berdasarkan : <br>
			Tanggal <b><?php echo tgl_indo2($tgl1); ?></b> s/d <b><?php echo tgl_indo2($tgl2) ?></b>
			<br>
            <br>
                
			</div>
            <?php
                }
                ?>
			<table border="1" class="tbl_rekap">
				<tr class="head">
					<td>NO</td><td>NAMA/NIP</td><td>PANGKAT/GOL</td><td>JABATAN</td><td class='cls6'>KEPERLUAN</td><td class='cls7'>TUJUAN</td><td class='cls8'>LAMA<br>PERJALANAN</td><td class='cls9'>TGL<br>BERANGKAT</td><td class='cls9'>TGL<br>KEMBALI</td><td>BERKENDARAAN</td><td class='cls10'>NOMOR &<br>TGL SPT</td><td class='cls11'>BIAYA SPT</td>
				</tr>
				<?php
				
				if(empty($bagian)){
				$query=mysql_query("SELECT * FROM tbl_kegiatan, tbl_provinsi, tbl_spt, tbl_pegawai WHERE tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_provinsi.id_provinsi=tbl_kegiatan.pergi AND tbl_pegawai.id_pegawai=tbl_spt.id_pegawai AND tbl_spt.status='N' AND tahun_ang='$mulai' GROUP BY tbl_spt.id_pegawai");
				}
				else{
				$query=mysql_query("SELECT * FROM tbl_kegiatan, tbl_provinsi, tbl_spt, tbl_pegawai WHERE tbl_pegawai.bagian='$bagian' AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_provinsi.id_provinsi=tbl_kegiatan.pergi AND tahun_ang='$mulai' GROUP BY tbl_spt.id_pegawai");
				}
				$no=1;
				while($r=mysql_fetch_array($query)){
				?>
				<tr class="body">
					<td class='cls1'><?php echo $no; ?></td><td class='cls2'><?php echo $r['nama']; ?><br><?php echo $r['nip']; ?></td><td class='cls3'><?php echo $r['pangkat']; ?> <br> (<?php echo $r['gol']; ?>)</td><td class='cls4'><div style="width:90px;text-align:center;"><?php echo $r['jabatan']; ?></div></td><td colspan='8' class='cls5'>
					<table border="0" class='child'>
						<?php
							$idPe=$r['id_pegawai'];
							$spt=$r['id_spt'];
							$qul=mysql_query("SELECT * FROM tbl_kegiatan, tbl_provinsi, tbl_spt, tbl_pegawai WHERE tbl_pegawai.id_pegawai=tbl_spt.id_pegawai AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_provinsi.id_provinsi=tbl_kegiatan.pergi AND tbl_spt.id_spt='$spt' AND tbl_spt.status='N' AND tahun_ang='$mulai'");
							$nn=1;
							while($rpul=mysql_fetch_array($qul)){
							
						?>
						
						<tr>
							<td style='width:0px'><?php echo $nn; ?></td>
							<td style='width:107px'><?php echo $rpul['keperluan']; ?></td>
							<td style='width:62px'><?php echo $rpul['kota']; ?></td>
							<td style='width:22px'><?php echo $rpul['lama_pd']; ?> Hari</td>
							
							<td style='width:13px'><?php echo $rpul['tglberangkat']; ?></td>
							<td style='width:16px'><?php echo $rpul['tglselesai']; ?></td>
							<td style='width:26px'><?php echo $rpul['berkendaraan']; ?></td>
							<td style='width:82px'><?php echo $rpul['no_kegiatan']; ?></td>
							<?php
				$hasil1=$rpul['setuju1']+$rpul['setuju2']+$rpul['setuju3']+$rpul['setuju4']+$rpul['setuju5']+$rpul['setuju6']+$rpul['setuju7']+$rpul['la3']+$rpul['lb3']+$rpul['lc3'];
                                $biayaspt=format_rupiah($hasil1);
				?>
							<td style='width:30px'><?php echo $biayaspt; ?></td>
						</tr>
						
						
						<?php
						$nn++;
						}
						?>
						
					</table>
					</td>
				</tr>
				<?php
				$no++;
				}
                $num=mysql_num_rows($query);

				?>
				<tr>
					<td colspan='11' style='font-size:8px;padding:5px;'>TOTAL</td>
					<td style='font-size:8px;padding:5px;'>
					
					<?php
                        if($num=='0'){
                            
                        }
                        else{
					$tot=mysql_query("SELECT sum(total_pengeluaran) AS total FROM tbl_spt WHERE status='N'");
					$rt=mysql_fetch_array($tot);
					echo format_rupiah($rt['total']);
                        }
                        ?>
					</td>
				</tr>
			</table>
			
			
			
			<table class="tbl_ttd" border="0px" style="margin-top:50px;">
				<tr>
				<?php
				$kua=$_GET['kua'];
				$querykom=mysql_query("SELECT * FROM tbl_pegawai WHERE id_pegawai='$kua'");
				$rl=mysql_fetch_array($querykom);
				?>
					<td class="cel1">
					Mengetahui<br>
					Kuasa Pengguna Anggaran<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<?php echo $rl['nama'] ?><br>
					NIP. <?php echo $rl['nip'] ?>
					
					</td>
					
                    <?php
				$kom=$_GET['kom'];
				$querykom=mysql_query("SELECT * FROM tbl_pegawai WHERE id_pegawai='$kom'");
				$rk=mysql_fetch_array($querykom);
				?>
					<td class="cel2">
					Medan, <?php echo tgl_indo(date('d-m-Y')); ?><br>
					Pejabat Pembuat Komitmen<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<?php echo $rk['nama'] ?><br>
					NIP. <?php echo $rk['nip'] ?>
					
					</td>
					
				</tr>
			</table>
		</div>
	</body>
</html>