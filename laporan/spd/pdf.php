<?php
include ("../../config/koneksi.php");
require("fpdf181/fpdf.php");
$nama_dinas = mysql_fetch_array(mysql_query("SELECT instansi.kepala_dinas,instansi.NIP,pegawai.pangkat,pegawai.tingkat,pegawai.golongan,pegawai.jabatan, pegawai.nama, instansi.nickname,instansi.nm_instansi,instansi.tempat, instansi.kd_instansi FROM pegawai,instansi WHERE pegawai.kd_instansi = instansi.kd_instansi AND pegawai.nip='$_GET[nip]'"));
$perihal = mysql_fetch_array(mysql_query("select * from ts where kd_instansi='$nama_dinas[kd_instansi]'"));
$spt = mysql_fetch_array(mysql_query("select * from spt WHERE no_spt = '$_GET[no_spt]'"));

$waktu = mysql_fetch_array(mysql_query("select datediff(tgl_selesai,tgl_mulai)AS durasi FROM spt WHERE no_spt='$_GET[no_spt]'"));
date_default_timezone_set('Asia/Kuala_Lumpur');
$date = date("d-M-Y");
$format_date = ("".$date."");
$pdf = new FPDF("P","cm","Legal");
$pdf->AddPage();
$pdf->SetFont("Times","B",14);
$pdf->Cell(0,0.3,"PEMERINTAH KABUPATEN TANAH LAUT",0,1,"C");
$pdf->Ln(0.3);
$pdf->Cell(0,0.3,"".strtoupper($nama_dinas['nm_instansi'])."",0,1,"C");
$pdf->Ln(0.3);
$pdf->SetFont("Times","",12);
$pdf->Cell(0,0.3,"Jl. A. Syairani Pelaihari Telp & Fax(0812)21035",0,1,"C");
$pdf->SetFont("Times","U",12);
$pdf->Ln(0.3);
$pdf->Cell(0,0.1,"_______________________________________________________________________________________",0,1,"C");
$pdf->Cell(0,0.3,"_______________________________________________________________________________________",0,1,"C");
$pdf->Ln(0.5);
$pdf->SetFont("Times","",12);
$pdf->SetX(15);
$pdf->Cell(0,0.3,"Lembar Ke   : ",0,1,"L");
$pdf->Ln(0.3);
$pdf->SetX(15);
$pdf->Cell(0,0.3,"Kode            : 904",0,1,"L");
$pdf->Ln(0.3);
$pdf->SetX(15);
$pdf->Cell(0,0.3,"Nomor          : ",0,1,"L");
$pdf->Ln(1);
$pdf->SetFont("Times","U",14);
$pdf->Cell(0,0.3,"SURAT PERINTAH PERJALANAN DINAS",0,1,"C");
$pdf->Ln(0.5);
$pdf->SetFont("Times","",12);
$pdf->Cell(0,0.3,"Nomor : 904 / 655 / SPPD / TL / ".$nama_dinas['nickname']."",0,1,"C");
$pdf->Ln();

$pdf->SetFont("Arial","",9);
$pdf->Cell(1,1,"1.",1,"C");
$pdf->Cell(9,1,"Pejabat berwenang yang memberikan perintah",1,"L");
$pdf->SetX(11);
$pdf->MultiCell(9.5,0.5,"Kepala ".$nama_dinas['nm_instansi']." Kabupaten Tanah Laut          ",1,"L");

$pdf->Cell(1,1,"2.",1,0,"L");
$pdf->Cell(9,1,"Nama pegawai yang diperintah / NIP",1,0,"L");
$pdf->SetX(11);
$pdf->MultiCell(9.5,1,"Kepala ".$nama_dinas['nama']." / ".$_GET['nip']."",1,"L");

$pdf->Cell(1,2.4,"3.",1,0,"C");
$pdf->MultiCell(9,0.8,"a. Pangkat/Gol. Meenurut PP No.6 Tahun 1997
b. Jabatan
c. Tingkat menurut peraturan perjalanan dinas",1,"L");
$pdf->SetY(9.6);
$pdf->SetX(11);
$pdf->MultiCell(9.5,0.8,"a. ".$nama_dinas['pangkat']."/".$nama_dinas['golongan']."
b. ".$nama_dinas['jabatan']."
c. ".$nama_dinas['tingkat']."",1,"L");


$pdf->Cell(1,2,"4.",1,0,"C");
$pdf->SetX(2);
$pdf->MultiCell(9,2,"Maksud perjalanan dinas",1,"L");
$pdf->SetY(12);
$pdf->SetX(11);
$pdf->MultiCell(9.5,2,"".$perihal['perihal']."",1,"L");

$pdf->Cell(1,1,"5.",1,0,"C");
$pdf->Cell(9,1,"Alat angkutan yang digunakan ",1,0,"L");
$pdf->Cell(9.5,1,"".$spt['transportasi']."",1,0,"L");
$pdf->Ln();

$pdf->Cell(1,1.6,"6.",1,0,"C");
// $pdf->SetX(2);
$pdf->MultiCell(9,0.8,"a. Tempat berangkat
b. Tempat tujuan",1,"L");
$pdf->SetY(15);
$pdf->SetX(11);
$pdf->MultiCell(9.5,0.8,"a. ".$nama_dinas['tempat']."
b. ".$spt['tempat']."",1,"L");


$pdf->Cell(1,2.4,"7.",1,0,"C");
$pdf->SetX(2);
$pdf->MultiCell(9,0.8,"a. Lamanya perjalanan dinas
b. Tanggal berangkat
c. Tanggal kembali",1,"L");
$pdf->SetY(16.6);
$pdf->SetX(11);
$pdf->MultiCell(9.5,0.8,"a. ".$waktu['durasi']." Hari
b. ".$spt['tgl_mulai']."
c. ".$spt['tgl_selesai']."",1,"L");

$pdf->Cell(1,1,"8.",1,0,"C");
$pdf->Cell(9,1,"Pengikut : Nama",1,0,"L");
$pdf->Cell(9.5,1,"-",1,0,"L");

$pdf->Ln();
$pdf->Cell(1,2.4,"9.",1,0,"C");
$pdf->SetX(2);
$pdf->MultiCell(9,0.8,"Pembebanan Anggaran
a. Instansi
b. Mata Anggaran",1,"L");
$pdf->SetY(20);
$pdf->SetX(11);
$pdf->MultiCell(9.5,0.8,"
a. ".$nama_dinas['nm_instansi']."
b. ".$spt['no_rek']."",1,"L");


$pdf->Cell(1,2,"10.",1,0,"C");
$pdf->Cell(9,2,"Keterangan",1,"L");
$pdf->Cell(9.5,2,"",1,"L");
$pdf->Ln(5);

$pdf->SetY(25);
$pdf->SetX(12.4);
$pdf->MultiCell(25,0.5,"Dikeluarkan di
Pada Tanggal",0,"L");
$pdf->SetY(25);
$pdf->SetX(16);
$pdf->MultiCell(25,0.5,":Pelaihari",0,"L");
$pdf->SetY(25.55);
$pdf->SetX(16);
$pdf->MultiCell(5,0.5,":".$format_date."",0,"L");
$pdf->Ln(0.5);

$pdf->SetFont("Arial","",11);
$pdf->SetX(12);
$pdf->MultiCell(7,0.5,"Plt.Kepala ".$nama_dinas['nm_instansi']." Kabupaten Tanah Laut,",0,"C");
$pdf->Ln(3);
$pdf->SetFont("Arial","U",11);
$pdf->SetX(12);
$pdf->MultiCell(7,0.5,"Plt.Kepala ".$nama_dinas['kepala_dinas']."",0,"C");
$pdf->SetFont("Arial","",10);
$pdf->SetX(12);
$pdf->MultiCell(7,0.5,"NIP. ".$nama_dinas['NIP']."",0,"C");
$pdf->Output();
?>
