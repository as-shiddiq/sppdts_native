<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_rupiah.php");
	include ("../../config/fungsi_indotgl.php");
	$no_spt=$_GET['no_spt'];
	$nip=$_GET['nip'];
	$id_spt = $_GET['no_spt'];
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		.garis{
			line-height:1.5px;
		}
		.garis .line1{
			border:1px solid #000;
		}
		.garis .line2{
			border:0.5px solid #000;
		}
		.logo{
				width:100px;
				height:100px;
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;
		
		}
		.kop .judul{
			text-align:center;
			
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}
		.tbl_lap1{
			margin:20px 0px;
		}
		.tbl_lap1 .cel1{
			width:190px;
		}
		.tbl_lap1 .cel2{
			width:200px;
		}
		.tbl_lap1 .cel3{
			width:290px;
		}
		.judul{
			margin:20px 0px;
			text-align:center;
		}
		.tbl_lap2{
			border-collapse:collapse;
			border:1px solid #000;
		}
		.tbl_lap2 .cel1{
			width:20px;
		}
		.tbl_lap2 .cel2{
			width:300px;
		}
		.tbl_lap2 .cel3{
			width:400px;
		}
		.tbl_lap2 td{
			padding:7px 2px;
		}
		.space{
			padding:10px;
		}
		</style>
	</head>
	<body>
		<div class="page">
		<?php
	
											$queryForm=mysql_query("SELECT * FROM spt, ts, pegawai, instansi WHERE spt.no_ts=ts.no_ts AND pegawai.kd_instansi=instansi.kd_instansi AND spt.nip=pegawai.nip AND spt.no_spt=$no_spt");
											$rF=@mysql_fetch_array($queryForm);
									
		
			//$queryTtd=mysql_query("SELECT * FROM ttd_spd WHERE id_ttd=1");
			//$rT=mysql_fetch_array($queryTtd);
											$nama_dinas = mysql_fetch_array(mysql_query("SELECT instansi.nm_instansi FROM pegawai,instansi WHERE pegawai.kd_instansi = instansi.kd_instansi AND nip='$_GET[nip]'"));
			?>
			
			<div class="judul">
			<b><h3>PEMERINTAH KABUPATEN TANAH LAUT</h3><br>
			<h3><?php echo $nama_dinas['nm_instansi'] ?></h3></b></div>
			<table class="tbl_lap1">
				<tr>
					<td class="cel1" align="center"></td>
					<td class="cel2"></td>
					<td class="cel3">
						<table>
							<tr>
								<td style="width:120px;">Lembar Ke</td><td>: </td>
							</tr>
							<tr>
								<td>Kode No</td><td>: 904 </td>
							</tr>
							<?php
							//$qun=mysql_query("SELECT * FROM tbl_spt WHERE id_spt='$id_spt'");
							//$rn=mysql_fetch_array($qun);
							?>
							<tr>
								<td>Nomor</td><td>: </td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<div class="judul"><u><b>SURAT PERJALANAN DINAS (SPD)</b></u></div>
			<table class="tbl_lap2" border="1px">
				<tr>
					<td class="cel1">1</td><td class="cel2">Pejabat berwenang yang memberi perintah</td><td class="cel3" colspan="3">
					<?php 
					$komi=$rF['pem_komitmen'];
					$queryKom=mysql_query("SELECT * FROM tbl_pegawai, tbl_spt WHERE tbl_pegawai.id_pegawai='$komi' AND tbl_pegawai.id_pegawai=tbl_spt.pem_komitmen");
					$rM=mysql_fetch_array($queryKom);
					echo $rM['nama'];
					?>
					</td>
				</tr>
				<tr>
					<td class="cel1">2</td><td class="cel2">Nama/NIP Pegawai yang melaksanakan<br>perjalanan dinas</td><td class="cel3" colspan="3"><?php echo $rF['nama']; ?> / <?php echo $rF['nip']; ?></td>
				</tr>
				<tr>
					<td class="cel1">3</td><td class="cel2">
					a. Pengkat dan Golongan<br>
					b. Jabatan / Instansi<br>
					c. Tingkat Biaya Perjalanan Dinas<br>
					</td><td class="cel3" colspan="3">
					a. <?php echo $rF['pangkat']; ?> / <?php echo $rF['golongan']; ?><br>
					b. <?php echo $rF['jabatan']; ?> / <?php echo $rF['instansi']; ?><br>
					c. <?php echo format_rupiah($rF['ting_biayapd']); ?><br>
					</td>
				</tr>
				<tr>
					<td class="cel1">4</td><td class="cel2">Maksud Perjalanan Dinas</td><td class="cel3" colspan="3"><?php echo $rF['tujuan']; ?></td>
				</tr>
				<tr>
					<td class="cel1">5</td><td class="cel2">Alat angkut yang depergunakan</td><td class="cel3" colspan="3"><?php echo $rF['transportasi']; ?></td>
				</tr>
				<tr>
					<td class="cel1">6</td><td class="cel2">
					a. Tempat berangkat<br>
					b. Tempat Tujuan<br>
					</td><td class="cel3" colspan="3">
					a. <?php echo $rF['tempat_tujuan']; ?><br>
					b. <?php echo $rF['kota']; ?><br>
					</td>
				</tr>
				<tr>
					<td class="cel1">7</td><td class="cel2">
					a. Lamanya Perjalanan Dinas<br>
					b. Tanggal berangkat<br>
					c. Tanggal harus kembali/tiba di tempat baru *)
					</td><td class="cel3" colspan="3">
					a. <?php echo $rF['lama_pd']." Hari"; ?><br>
					b. <?php echo tgl_indo($rF['tgl_mulai']); ?><br>
					c. <?php echo tgl_indo($rF['tgl_selesai']); ?><br>
					</td>
				</tr>
				<tr>
					<td class="cel1">8</td><td class="cel2"><b>Pengikut : </b> Nama</td><td align="center">Tanggal Lahir</td><td align="center">Keterangan</td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">
						<?php
													$quPeng=mysql_query("SELECT * FROM tbl_pengikut WHERE id_spt=$id_spt");
												$no=1;
													while($rP=mysql_fetch_array($quPeng)){
														echo $no.". ".$rP['nama_pengikut']."<br>";
													$no++;
													}
												?>
					</td>
					<td>
						<?php
												$quPeng=mysql_query("SELECT * FROM tbl_pengikut WHERE id_spt=$id_spt");
												
													while($rL=mysql_fetch_array($quPeng)){
														echo tgl_indo($rL['tgl_lahir'])."<br>";
													
													}
												?>
					</td>
					<td>
						<?php
												$quPeng=mysql_query("SELECT * FROM tbl_pengikut WHERE id_spt=$id_spt");
												
													while($rK=mysql_fetch_array($quPeng)){
														echo $rK['ket']."<br>";
													
													}
												?>
					</td>
					
				</tr>
				<tr>
					<td class="cel1">9</td><td class="cel2">
					Pembebanan Anggaran<br>
					a. Instansi<br>
					b. Akun<br>
					
					</td><td class="cel3" colspan="3">
					<br>
					a. KANTOR DINAS MARTECH<br>
					b. <?php echo $rF['akun'] ?><br>
				
					</td>
				</tr>
				<tr>
					<td class="cel1">10</td><td class="cel2">Keterangan lain-lain</td><td class="cel3" colspan="3">
					<?php echo $rF['ket_lain'] ?><br>
					<?php echo $rF['ket_lain2'] ?><br>
					<?php echo $rF['ket_lain3'] ?></td>
				</tr>
			</table>
			<p class="space"></p>
			<table class="tbl_lap2" border="0px">
			
				<tr>
					<td class="cel1"></td><td class="cel2">coret yang tidak perlu</td><td class="cel3">
					Dikeluarkan di : <?php// echo $rT['dikeluarkandi']; ?><br>
					Tanggal : <?php// echo tgl_indo($rT['tanggal']); ?><br>
					<br>
					Pejabat Pembuat Komitmen :<br>
					<?php //echo $rT['komitmen']; ?>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					
					<?php echo $rM['nama']; ?><br>
					NIP . <?php echo $rM['nip']; ?>
					</td>
				</tr>
			</table>
			
		</div>
	</body>
</html>