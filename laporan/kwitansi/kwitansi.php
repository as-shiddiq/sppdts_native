<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	include ("../../config/fungsi_rupiah.php");
	include ("../../config/fungsi_terbilang.php");
	$tgl_sek=$_POST['tgl_sekarang'];
	$id_spt=$_GET['id_spt'];
	$bendahara=$_POST['bendahara'];
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		.logo{
			width:150px;
			height:150px;
		}
		.kop{
			margin-top:30px;
		}
		.kop .cel1{
			width:350px;
		}
		.kop .cel2{
			width:120px;
			text-align:center;
			font-size:13px;
		}
		.kop .cel3{
			width:20px;
			text-align:left;
			font-size:13px;
		}
		.kop .cel4{
			width:290px;
			font-size:14px;
		}
		
		.tbl_lap1 .cel1{
			width:320px;
		}
		.tbl_lap1 .cel2{
			width:80px;
		}
		.tbl_lap1 .cel3{
			width:220px;
		}
		.isi_1{
			margin-top:30px;
		}
		.isi_1 .cel1{
			width:80px;
		}
		.isi_1 .cel2{
			width:10px;
		}
		.isi_1 .cel3{
			width:60px;
		}
		.judul{
			margin:40px 0px;
			text-align:center;
			font-size:20px;
		}
		.box{
			width:745px;
			border:4px double #000;
		}
		
		.tbl_lap2 .cel1{
			width:130px;
		}
		.tbl_lap2 .cel2{
			width:5px;
		}
		.tbl_lap2 .cel3{
			width:400px;
		}
		.tbl_lap2 td{
			padding:8px 5px;
		}
		.tbl_lap3{
			
			margin-top:100px;
		}
		.tbl_lap3 .cel1{
			width:320px;
		}
		.tbl_lap3 .cel2{
			width:400px;
			text-align:center;
		}
		.tbl_ttd .cel1{
			width:360px;
		}
		.tbl_ttd .cel2{
			width:360px;
		}
		.tbl_ttd td{
			line-height:20px;
		}
		</style>
	</head>
	<body>
		<div class="page">
			<table class="kop">
				<tr>
					<td class="cel1"></td>
					<td class="cel2">
					Lampiran 3
					</td>
					<td class="cel3">:</td>
					<td class="cel4">
					Perdirjen. Perbendaharaan<br>
					Nomor : PER-66/PB/2005<br>
					Tanggal : 28 Desember 2005
					</td>
				</tr>
			</table>
			
			<div class="judul"><b>KWITANSI UP</b></div>
			
			
			<div class="box">
				<table class="tbl_lap1">
					<tr>
						<td class="cel1"></td>
						<td class="cel2"></td>
						<td class="cel3">
						<?php
						$querykw=mysql_query("SELECT * FROM kwitansi WHERE id_kwitansi='1'");
						$rkw=mysql_fetch_array($querykw);
						?>
							<table class="isi_1">
								<tr>
									<td class="cel1">T A</td><td class="cel2">:</td><td class="cel3"> <?php echo $rkw['ta']; ?></td>
								</tr>
								<tr>
									<td>No. Bukti</td><td>:</td><td> <?php echo $rkw['no']; ?></td>
								</tr>
								<tr>
									<td>M A K</td><td>:</td><td><?php echo $rkw['mak']; ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="judul"><u><b>KWITANSI / BUKTI PEMBAYARAN</b></u></div>
				<table class="tbl_lap2">
					<tr>
						<td class="cel1">Sudah terima dari</td><td class="cel2">:</td><td class="cel3">KANTOR OTORITAS BANDAR UDARA WILAYAH II</td>
					</tr>
					<tr>
						<td class="cel1">Jumlah Uang</td><td class="cel2">:</td><td class="cel3">
						<?php
							$queryForm=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_pegawai WHERE tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
							$rF=mysql_fetch_array($queryForm);
							$hasil=$rF['setuju1']+$rF['setuju2']+$rF['setuju3']+$rF['setuju4']+$rF['setuju5']+$rF['setuju6']+$rF['setuju7']+$rF['la3']+$rF['lb3']+$rF['lc3'];
							echo format_rupiah($hasil);
						?>
						</td>
					</tr>
					<tr>
						<td class="cel1">Terbilang</td><td class="cel2">:</td><td class="cel3"><i><?php echo Terbilang($hasil); ?> Rupiah</i></td>
					</tr>
					<tr>
						<td class="cel1">Untuk</td><td class="cel2">:</td><td class="cel3"><?php echo $rF['keperluan']; ?></td>
					</tr>
				</table>
				
				<table class="tbl_lap3">
					<tr>
						<td class="cel1"></td><td class="cel2">
						Medan, <?php echo tgl_indo($tgl_sek); ?><br>
						Yang Menerima,<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
							<?php
			$query=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_pegawai WHERE tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
			$rA=mysql_fetch_array($query);
			echo $rA['nama']."<br>"; ?>
						<?php echo "NIP ".$rA['nip']; ?>
						</td>
					</tr>
				</table>
				<div class="space"></div>
				<hr>
				<?php
							$ttd1=mysql_query("SELECT * FROM tbl_spt, tbl_pegawai WHERE tbl_spt.pem_komitmen=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
							$t=mysql_fetch_array($ttd1);
						?>
					
				<table class="tbl_ttd">
					<tr>
						<td class="cel1" align="center">
							Setuju Dibayar,<br>
							<b>PEJABAT PEMBUAT KOMITMEN</b><br>
							<br>
							<br>
							<br>
							<br>
							
							<br>
							<?php echo $t['nama']; ?><br>
							NIP. <?php echo $t['nip']; ?>
						</td>
						<td  class="cel2" align="center">
							Setuju Dibayar,<br>
							<b>BENDAHARA PENGELUARAN</b><br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<?php
							$quer=mysql_query("SELECT * FROM tbl_pegawai WHERE id_pegawai='$bendahara'");
							$rb=mysql_fetch_array($quer);
							?>
							<?php echo $rb['nama']; ?><br>
							NIP. <?php echo $rb['nip']; ?>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>