<?php
	include ("../../config/koneksi.php");
	include ("../../config/fungsi_indotgl.php");
	include ("../../config/fungsi_rupiah.php");
	$id_spt=$_POST['id_spt'];
	$tgl_sek=$_POST['tgl_sekarang'];
	?>
<!doctype html>
<html>
	<head>
		<link rel="shortcut icon" href="../img/laporan.png">
		<link rel="stylesheet" type="text/css" href="../../css/laporan.css">
		<style>
		.logo{
				width:100px;
				height:100px;
			}
		.kop{
			border-collpase:collapse;
		}
		.kop .header{
			width:130px;
		
		}
		.kop .judul{
			text-align:center;
			
		}
		.kop table{
			text-align:left;
			border-collpase:collapse;
		}
		.kop table .cel1{
			width:200px;
		}
		.kop table .cel2{
			width:170px;
		}
		.kop table .cel3{
			width:170px;
		}
		.kop table td{
			font-size:11px;
		}
		
	
			
			.garis{
				line-height:1.5px;
			}
			.garis .line1{
				border:1px solid #000;
			}
			.garis .line2{
				border:0.5px solid #000;
			}
		.tbl_lap1{
			margin:20px 0px;
		}
		.tbl_lap1 .cel1{
			width:180px;
		}
		.tbl_lap1 .cel2{
			width:200px;
		}
		.tbl_lap1 .cel3{
			width:290px;
		}
		.judul{
			margin:20px 0px;
			text-align:center;
		}
		.tbl_lap2{
			border-collapse:collapse;
			border:1px solid #000;
		}
		.tbl_lap2 .cel1{
			width:20px;
		}
		.tbl_lap2 .cel2{
			width:200px;
		}
		.tbl_lap2 .cel3{
			width:400px;
		}
		.tbl_lap2 td{
			padding:5px 2px;
		}
		.space{
			padding:10px;
		}
		.tbl_lap3 .cel1{
			width:300px;
		}
		.tbl_lap3 .cel2{
			width:100px;
		}
		.tbl_lap3 .cel3{
			width:300px;
		}
		</style>
	</head>
	<body>
		<div class="page">
			<div class="kops">
			<table class="kop">
				<tr>
					<td class="header">
						<img src="../../images/logo_perhunungan.png" class="logo"> 
					</td>
					<td  class="judul">
						<H3>SISTEM PERJALANAN DINAS PADA</H3>
						<h2>KANTOR DINAS MARTECH</h2>
                        
						<table>
							<tr>
								<td class="cel1">Jalan : <br>
								Medan, Indonesia
								</td>
								<td class="cel2">Telepon : 061-xxxx<br>
								061 - xxxx xxxx Ext xxxx<br>
								061 - xxxx xxxx Ext xxxx
								</td>
								<td class="cel3">Fax : 061-xxxxxxxx<br>
								Email : perjalanandinas@gmail.com
								</td>
							</tr>
						
						</table>
		
					</td>
				</tr>
			</table>
		</div>
		<div class="garis">
			<hr class="line1"/>
			<hr class="line2"/>
		</div>
			<?php
			$query=mysql_query("SELECT * FROM tbl_pembebanan, tbl_pegawai WHERE tbl_pembebanan.id_spt='$id_spt' AND tbl_pegawai.id_pegawai=tbl_pembebanan.id_pempetnyataandipa ORDER BY tbl_pembebanan.id_pembebanan DESC");
			$r=mysql_fetch_array($query);
			?>
			<div class="judul"><u><b>SURAT PERYATAAN PEMBEBANAN</b></u><br> <u><b>BIAYA PEMBATALAN PERJALANAN DINAS JABATAN</b></u></div>
			<table class="tbl_lap2">
				<tr>
					<td colspan="3">Yang bertanda tanggan dibawah ini:</td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Nama</td><td class="cel3">: <?php echo $r['nama'];?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">NIP</td><td class="cel3">:  <?php echo $r['nip'];?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Jabatan</td><td class="cel3">:  Pejabat Pembuat Komitmen</td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Satker</td><td class="cel3">:  <?php echo $r['instansi'];?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Kementrian/Lembaga</td><td class="cel3">: Kementerian Perhubungan</td>
				</tr>
				
				<?php
			$queryForm=mysql_query("SELECT * FROM tbl_spt, tbl_kegiatan, tbl_pegawai WHERE tbl_spt.id_kegiatan=tbl_kegiatan.id_kegiatan AND tbl_spt.id_pegawai=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=$id_spt");
			$rF=mysql_fetch_array($queryForm);
		?>
				<tr>
					<td colspan="3">menyatakan dengan sesunggunhnya, bahwa Perjalanan Dinas Jabatan berdasarkan Surat Tugas Nomor: <b><?php echo $rF['no_kegiatan']; ?></b> tanggal <b><?php echo tgl_indo($rF['tgl_spt']); ?></b> dan SPD Nomor <b> <?php echo $rF['nomor_sppd']; ?></b> tanggal <b><?php echo tgl_indo($rF['tgl_modify']); ?></b> atas nama:</td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Nama</td><td class="cel3">: <?php  echo $rF['nama']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">NIP</td><td class="cel3">:  <?php  echo $rF['nip']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Jabatan</td><td class="cel3">:  <?php  echo $rF['jabatan']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Satker</td><td class="cel3">:  <?php  echo $rF['instansi']; ?></td>
				</tr>
				<tr>
					<td class="cel1"></td><td class="cel2">Kementrian/Lembaga</td><td class="cel3">: Kementrian Perhubungan</td>
				</tr>
				
				<tr>
					<td colspan="3" style="text-align:justify">dibatakan sesuai dengan surat Pernyataan Pembatalan Tugas Perjalanan Dinas Jabatan Nomor <br><b><?php  echo $rF['nomor_sppd']; ?></b> tanggal <b><?php  echo $rF['tgl_spt']; ?></b>. 
					<?php
				$query2=mysql_query("SELECT * FROM tbl_pembebanan WHERE id_spt='$id_spt' ORDER BY id_pembebanan DESC");
				$q=mysql_fetch_array($query2);
				?>
					Berkenan dengan pembatalan tersebut, biaya transpor berupa <b><?php echo format_rupiah($q['transport']); ?></b> dan biaya penginapan yang telah terlanjur dibawaykan atas beban DIPA tidak dapat dikembalikan / <i>refund</i><br> (sebagian/seluruhnya) sebesar  <b><?php echo format_rupiah($q['total']); ?></b>  sehingga dibebankan pada DIPA Nomor:  <b><?php echo $q['no_dipa']; ?></b>  tanggal  <b><?php echo $q['tgl_dipa']; ?></b>  Satker  <b><?php echo $q['satker']; ?></b>  (13).
					<br>
					<br>
					Demikian surat pernyataan ini dibuat dengan sebenarnya dan apabila dikemudian hari ternyata surat pernyataan ini tidak benar dan menimbulkan kerugian negara, saya bertanggung jawab penuh dan bersedia menyetorkan kerugian negara tersebut ke Kas Negara.
					</td>
				</tr>
			</table>
			<p class="space"></p>
			<table class="tbl_lap3" border="0px">
				<?php
				$ttd=mysql_query("SELECT * FROM tbl_spt, tbl_pembebanan, tbl_pegawai WHERE tbl_pembebanan.id_pempetnyataandipa=tbl_pegawai.id_pegawai AND tbl_spt.id_spt=tbl_pembebanan.id_spt");
				$rt=mysql_fetch_array($ttd);
				?>
				<tr>
					<td class="cel1"></td>
						<td class="cel2"></td><td class="cel3">
					Medan, <?php echo tgl_indo($tgl_sek); ?><br>
					<br>
					Yang Membuat Pernyataan<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<?php echo $rt['nama']; ?><br>
					NIP. <?php echo $rt['nip']; ?><br>
					</td>
				</tr>
			</table>
			
		</div>
	</body>
</html>