<?php  

function generate_pdf($html='',$nama_file='',$paper='',$orient=''){

	include("dompdf/dompdf_config.inc.php");
  if($nama_file==''){
  	$nama_file='report';
  }
  if($paper==''){
  	$paper='legal';
  }
  if($orient==''){
  	$orient='landscape';
  }
  $dompdf = new DOMPDF();
  $dompdf->load_html($html);
  $dompdf->set_paper($paper, $orient);
  $dompdf->render();

  $dompdf->stream($nama_file.".pdf", array("Attachment" => false));

}
